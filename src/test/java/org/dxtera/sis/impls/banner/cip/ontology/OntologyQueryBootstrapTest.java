package org.dxtera.sis.impls.banner.cip.ontology;

// NOTICE: THIS CLASS IS GENERATED BY THE build_dxtera` REPOSITORY.
//         ANY DIRECT CHANGES TO THIS CODE WILL BE OVERWRITTEN.
//         TO REQUEST CHANGES, FILE A PR WITH AN EXAMPLE OF THE CHANGE
//         YOU WOULD LIKE TO SEE.

import static org.junit.Assert.*;
import static org.junit.Assume.*;

import net.okapia.osid.kilimanjaro.BootLoader;
import net.okapia.osid.kilimanjaro.OsidVersions;
import net.okapia.osid.primordium.id.BasicId;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.osid.*;
import org.osid.ontology.*;

public class OntologyQueryBootstrapTest {

  protected org.osid.ontology.OntologyManager mgr;

  // baseMgr used for the Id sanity tests for Lookup sessions
  protected org.osid.ontology.OntologyManager baseMgr;

  // NOTICE: THIS METHOD IS GENERATED BY THE build_dxtera` REPOSITORY.
  //         ANY DIRECT CHANGES TO THIS CODE WILL BE OVERWRITTEN.
  //         TO REQUEST CHANGES, FILE A PR WITH AN EXAMPLE OF THE CHANGE
  //         YOU WOULD LIKE TO SEE.
  // set up the ContactManager
  @Before
  public void setUpBeforeClass() {
    try {
      BootLoader loader = new BootLoader();
      OsidRuntimeManager runtime = loader.getRuntimeManager("test");

      baseMgr =
          (org.osid.ontology.OntologyManager)
              runtime.getManager(
                  OSID.ONTOLOGY,
                  "org.dxtera.sis.impls.banner.cip.ontology.BannerOntologyManager",
                  OsidVersions.V3_0_0.getVersion());
      mgr =
          (org.osid.ontology.OntologyManager)
              runtime.getManager(
                  OSID.ONTOLOGY,
                  "org.dxtera.sis.impls.banner.cip.ontology.BannerOntologyManager",
                  OsidVersions.V3_0_0.getVersion());

    } catch (UnsupportedException e) {
      dump(e);
      assumeNoException("ContactManager not supported", e);
    } catch (OperationFailedException e) {
      dump(e);
      fail("Caught OperationFailedException.");
    } catch (NotFoundException e) {
      dump(e);
      fail("Implementation string NotFoundException:\n" + e.getMessage());
    } catch (NullArgumentException e) {
      dump(e);
      fail("Caught NullArgumentException.");
    } catch (ConfigurationErrorException e) {
      dump(e);
      fail("Caught ConfigurationErrorException.");
    } catch (Exception e) {
      dump(e);
      fail("Caught unexpected exception:\n" + e.getMessage());
    }
  }

  // NOTICE: THIS METHOD IS GENERATED BY THE build_dxtera` REPOSITORY.
  //         ANY DIRECT CHANGES TO THIS CODE WILL BE OVERWRITTEN.
  //         TO REQUEST CHANGES, FILE A PR WITH AN EXAMPLE OF THE CHANGE
  //         YOU WOULD LIKE TO SEE.
  private void dump(Throwable t) {
    while (t != null) {
      System.out.println(t.getClass() + ": " + t.getMessage());
      t.printStackTrace();
      t = t.getCause();
      if (t != null) {
        System.out.print("Caused by ");
      }
    }
  }

  // NOTICE: THIS METHOD IS GENERATED BY THE build_dxtera` REPOSITORY.
  //         ANY DIRECT CHANGES TO THIS CODE WILL BE OVERWRITTEN.
  //         TO REQUEST CHANGES, FILE A PR WITH AN EXAMPLE OF THE CHANGE
  //         YOU WOULD LIKE TO SEE.
  @After
  public void tearDownAfterClass() {
    mgr.close();
    baseMgr.close();
  }

  // Tests querySubjectId
  // NOTICE: THIS TEST IS GENERATED BY THE build_dxtera` REPOSITORY.
  //         ANY DIRECT CHANGES TO THIS CODE WILL BE OVERWRITTEN.
  //         TO REQUEST CHANGES, FILE A PR WITH AN EXAMPLE OF THE CHANGE
  //         YOU WOULD LIKE TO SEE.
  @Ignore
  @Test
  public void testSubjectQueryall() {
    try {
      org.osid.ontology.SubjectQuerySession session = mgr.getSubjectQuerySession();
      org.osid.ontology.SubjectQuery query = session.getSubjectQuery();
      org.osid.ontology.SubjectList results = session.getSubjectsByQuery(query);
      assertTrue(results.available() > 0);
      int i = 0;
      while (results.hasNext()) {
        Subject c = results.getNextSubject();
        System.out.println("SubjectID = " + c.getId());
        System.out.println("GenusType = " + c.getGenusType());
        System.out.println("Description = " + c.getDescription());
      }
    } catch (Exception e) {
      dump(e);
      fail("Caught unexpected exception:\n" + e.getMessage());
    }
  }

  @Test
  public void testRelevancyQueryall() {
    try {
      org.osid.ontology.RelevancyQuerySession session = mgr.getRelevancyQuerySession();
      org.osid.ontology.RelevancyQuery query = session.getRelevancyQuery();
      org.osid.ontology.RelevancyList results = session.getRelevanciesByQuery(query);
      assertTrue(results.available() > 0);
      int i = 0;
      while (results.hasNext()) {
        Relevancy c = results.getNextRelevancy();
        System.out.println("SubjectId =" + c.getSubjectId());
        System.out.println("RelevancyID = " + c.getId());
        System.out.println("GenusType = " + c.getGenusType());
        System.out.println("MappedId = " + c.getMappedId());
      }
    } catch (Exception e) {
      dump(e);
      fail("Caught unexpected exception:\n" + e.getMessage());
    }
  }

  @Ignore
  @Test
  public void testSubjectQueryResourceMatch()
      throws org.osid.OperationFailedException, org.osid.PermissionDeniedException {
    try {
      org.osid.ontology.SubjectQuerySession session = mgr.getSubjectQuerySession();
      org.osid.ontology.SubjectQuery query = session.getSubjectQuery();
      // query.matchResourceId(BasicId.valueOf("Resource:195332@CCSNH"), true);
      // query.matchResourceId(BasicId.valueOf("Resource:399967@CCSNH"), true);
      // query.matchResourceId(BasicId.valueOf("Resource:424289@CCSNH"), true);
      query.matchId(BasicId.valueOf("Subject:F-GENDER@CCSNH"), true);
      query.matchId(BasicId.valueOf("Subject:M-GENDER@CCSNH"), true);
      query.matchId(BasicId.valueOf("Subject:N-GENDER@CCSNH"), true);
      org.osid.ontology.SubjectList results = session.getSubjectsByQuery(query);
      assertTrue(results.available() > 0);
      int i = 0;
      while (results.hasNext()) {
        Subject c = results.getNextSubject();
        System.out.println("SubjectID = " + c.getId());
        System.out.println("GenusType = " + c.getGenusType());
        System.out.println("Description = " + c.getDescription());
      }
    } catch (Exception e) {
      dump(e);
      fail("Caught unexpected exception:\n" + e.getMessage());
    }
  }

  @Test
  public void testRelevancyQueryResourceMatch()
      throws org.osid.OperationFailedException, org.osid.PermissionDeniedException {
    try {
      org.osid.ontology.RelevancyQuerySession session = mgr.getRelevancyQuerySession();
      org.osid.ontology.RelevancyQuery query = session.getRelevancyQuery();
      // query.matchResourceId(BasicId.valueOf("Resource:195332@CCSNH"), true);
      // query.matchResourceId(BasicId.valueOf("Resource:399967@CCSNH"), true);
      // query.matchResourceId(BasicId.valueOf("Resource:424289@CCSNH"), true);
      query.matchId(BasicId.valueOf("Relevancy:1000-cip.2010.52.0208@CCSNH"), true);
      query.matchId(BasicId.valueOf("Relevancy:1001-cip.2010.13.1501@CCSNH"), true);
      query.matchId(BasicId.valueOf("Relevancy:1002-cip.2010.13.1501@CCSNH"), true);
      org.osid.ontology.RelevancyList results = session.getRelevanciesByQuery(query);
      assertTrue(results.available() > 0);
      int i = 0;
      while (results.hasNext()) {
        Relevancy c = results.getNextRelevancy();
        System.out.println("SubjectId =" + c.getSubjectId());
        System.out.println("RelevancyID = " + c.getId());
        System.out.println("GenusType = " + c.getGenusType());
        System.out.println("MappedId = " + c.getMappedId());
      }
    } catch (Exception e) {
      dump(e);
      fail("Caught unexpected exception:\n" + e.getMessage());
    }
  }

  /* @Test
  public void testAddressQueryResourceExtMatch()
      throws OperationFailedException, PermissionDeniedException {
    // try {
    org.osid.ontology.cip.AddressQuerySession session = mgr.getAddressQuerySession();
    org.osid.ontology.cip.AddressQuery query = session.getAddressQuery();
    query.matchResourceId(BasicId.valueOf("Resource:195332@CCSNH"), true);
    query.matchResourceId(BasicId.valueOf("Resource:399967@CCSNH"), true);
    query.matchResourceId(BasicId.valueOf("Resource:424289@CCSNH"), true);
    org.osid.ontology.cip.AddressList results = session.getAddressesByQuery(query);
    assertTrue(results.available() > 0);
    int i = 0;
    while (results.hasNext()) {
      Address c = results.getNextAddress();
      // System.out.println("RT " + (c.getRecordTypes().toString()));
      Type t;
      TypeList urt = c.getRecordTypes();
      while (urt.hasNext()) {
        t = urt.getNextType();
        // System.out.println("IsTypeBean?" + (t instanceof TypeBean) + (t instanceof Type) + t);
        AddressRecord r;
        //  System.out.println("RT Type : " +  t);
        switch (t.getIdentifier()) {
          case "PhysicalAddressRecord":
            System.out.println("ExtType " + " " + t.getIdentifier());
            r = (AddressRecord) c.getAddressRecord(t);
            GenericRecord gr = (GenericRecord) r;
            System.out.println("Street :" + gr.getElementString("street"));
            System.out.println("City :" + gr.getElementString("city"));
            System.out.println("State :" + gr.getElementString("state"));
            System.out.println("Zip :" + gr.getElementString("zip"));
            System.out.println("Nation :" + gr.getElementString("nation"));
            System.out.println(gr.getElements());
            break;
          case "PostalAddressRecord":
            // org.dxtera.sis.impls.banner.ontology.cip.PostalAddressInterface
            // System.out.println("ExtType " + " " + t.toString());
            r = (AddressRecord) c.getAddressRecord(t);
            PostalAddressInterface pr = (PostalAddressInterface) r;
            System.out.println("Mailstop: " + pr.getMailstop());
            System.out.println("Streets: " + Arrays.toString(pr.getStreets()));
            System.out.println("Locality: " + pr.getLocality());
            System.out.println("CountyOrParish: " + pr.getCountyOrParish());
            System.out.println("PostalCode: " + pr.getPostalCode());
            System.out.println("Country: " + pr.getCountry());
        }

        System.out.println("ResourceID = " + c.getResourceId());
        System.out.println("AddressID = " + c.getId());
        System.out.println("GenusType = " + c.getGenusType());
        System.out.println("AddressText = " + c.getAddressText());
      }
    } /* }catch (Exception e) {
        dump(e);
        e.printStackTrace();
        fail("Caught unexpected exception:\n" + e.toString());
      }
  }

  @Test
  public void testAddressQueryMultiMatch() {
    try {
      org.osid.ontology.cip.AddressQuerySession session = mgr.getAddressQuerySession();
      org.osid.ontology.cip.AddressQuery query = session.getAddressQuery();
      query.matchResourceId(BasicId.valueOf("Resource:424289@CCSNH"), true);
      query.matchId(BasicId.valueOf("Address:424289-ADD-MA-3@CCSNH"), true);
      //query.matchGenusType(BasicType.valueOf("Type:PersonAddressMailing@CCSNH"), true);
      org.osid.ontology.cip.AddressList results = session.getAddressesByQuery(query);
      assertTrue(results.available() > 0);
      int i = 0;
      while (results.hasNext()) {
        Address c = results.getNextAddress();
        System.out.println("ResourceID = " + c.getResourceId());
        System.out.println("AddressID = " + c.getId());
        System.out.println("GenusType = " + c.getGenusType());
        System.out.println("AddressText = " + c.getAddressText());
      }
    } catch (Exception e) {
      dump(e);
      fail("Caught unexpected exception:\n" + e.getMessage());
    }
  } */
}
