package org.dxtera.sis.impls.banner.cip.ontology;

// NOTICE: THIS CLASS IS GENERATED BY THE build_dxtera` REPOSITORY.
//         ANY DIRECT CHANGES TO THIS CODE WILL BE OVERWRITTEN.
//         TO REQUEST CHANGES, FILE A PR WITH AN EXAMPLE OF THE CHANGE
//         YOU WOULD LIKE TO SEE.

import static org.junit.Assert.*;
import static org.junit.Assume.*;

import net.okapia.osid.kilimanjaro.BootLoader;
import net.okapia.osid.kilimanjaro.OsidVersions;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.osid.*;
import org.osid.id.*;
import org.osid.ontology.*;
import org.osid.type.*;

public class RelevancyFormTest {

  protected OntologyManager mgr;

  // NOTICE: THIS METHOD IS GENERATED BY THE build_dxtera` REPOSITORY.
  //         ANY DIRECT CHANGES TO THIS CODE WILL BE OVERWRITTEN.
  //         TO REQUEST CHANGES, FILE A PR WITH AN EXAMPLE OF THE CHANGE
  //         YOU WOULD LIKE TO SEE.
  // set up the OntologyManager
  @Before
  public void setUp() {
    try {
      BootLoader loader = new BootLoader();
      OsidRuntimeManager runtime = loader.getRuntimeManager("test");

      mgr =
          (OntologyManager)
              runtime.getManager(
                  OSID.ONTOLOGY,
                  "org.dxtera.sis.impls.banner.cip.ontology.BannerOntologyManager",
                  OsidVersions.V3_0_0.getVersion());
    } catch (UnsupportedException e) {
      dump(e);
      assumeNoException("OntologyManager not supported", e);
    } catch (OperationFailedException e) {
      dump(e);
      fail("Caught OperationFailedException.");
    } catch (NotFoundException e) {
      dump(e);
      fail("Implementation string NotFoundException:\n" + e.getMessage());
    } catch (NullArgumentException e) {
      dump(e);
      fail("Caught NullArgumentException.");
    } catch (ConfigurationErrorException e) {
      dump(e);
      fail("Caught ConfigurationErrorException.");
    } catch (Exception e) {
      dump(e);
      fail("Caught unexpected exception:\n" + e.getMessage());
    }
  }

  // NOTICE: THIS METHOD IS GENERATED BY THE build_dxtera` REPOSITORY.
  //         ANY DIRECT CHANGES TO THIS CODE WILL BE OVERWRITTEN.
  //         TO REQUEST CHANGES, FILE A PR WITH AN EXAMPLE OF THE CHANGE
  //         YOU WOULD LIKE TO SEE.
  private void dump(Throwable t) {
    while (t != null) {
      System.out.println(t.getClass() + ": " + t.getMessage());
      t.printStackTrace();
      t = t.getCause();
      if (t != null) {
        System.out.print("Caused by ");
      }
    }
  }

  // NOTICE: THIS METHOD IS GENERATED BY THE build_dxtera` REPOSITORY.
  //         ANY DIRECT CHANGES TO THIS CODE WILL BE OVERWRITTEN.
  //         TO REQUEST CHANGES, FILE A PR WITH AN EXAMPLE OF THE CHANGE
  //         YOU WOULD LIKE TO SEE.
  @After
  public void tearDown() {
    mgr.close();
  }

  // NOTICE: THIS METHOD IS GENERATED BY THE build_dxtera` REPOSITORY.
  //         ANY DIRECT CHANGES TO THIS CODE WILL BE OVERWRITTEN.
  //         TO REQUEST CHANGES, FILE A PR WITH AN EXAMPLE OF THE CHANGE
  //         YOU WOULD LIKE TO SEE.
  private RelevancyAdminSession getRelevancyAdminSession() {
    RelevancyAdminSession session = null;
    try {
      session = mgr.getRelevancyAdminSession();
    } catch (OperationFailedException e) {
      dump(e);
      fail("Caught OperationFailedException.");
    } catch (UnimplementedException e) {
      dump(e);
      assumeNoException("RelevancyAdminSession not supported", e);
    } catch (Exception e) {
      dump(e);
      fail("Caught unexpected exception:\n" + e.getMessage());
    }
    return session;
  }

  // NOTICE: THIS METHOD IS GENERATED BY THE build_dxtera` REPOSITORY.
  //         ANY DIRECT CHANGES TO THIS CODE WILL BE OVERWRITTEN.
  //         TO REQUEST CHANGES, FILE A PR WITH AN EXAMPLE OF THE CHANGE
  //         YOU WOULD LIKE TO SEE.
  private RelevancyForm getRelevancyFormForCreate(RelevancyAdminSession session) {

    RelevancyForm createForm = null;
    Id fakeId = new net.okapia.osid.primordium.id.BasicId("JUnit", "Test", "Id");
    try {
      createForm = session.getRelevancyFormForCreate(fakeId, fakeId, new Type[0]);
    } catch (PermissionDeniedException e) {
      dump(e);
      fail("Caught PermissionDeniedException.");
    } catch (OperationFailedException e) {
      dump(e);
      fail("Caught OperationFailedException.");
    } catch (NullArgumentException e) {
      dump(e);
      fail("Caught NullArgumentException.");
    } catch (UnsupportedException e) {
      dump(e);
      fail("Caught UnsupportedException.");
    } catch (Exception e) {
      dump(e);
      fail("Caught unexpected exception:\n" + e.getMessage());
    }
    return createForm;
  }

  // NOTICE: THIS METHOD IS GENERATED BY THE build_dxtera` REPOSITORY.
  //         ANY DIRECT CHANGES TO THIS CODE WILL BE OVERWRITTEN.
  //         TO REQUEST CHANGES, FILE A PR WITH AN EXAMPLE OF THE CHANGE
  //         YOU WOULD LIKE TO SEE.
  // Tests getDisplayNameMetadata
  @Test
  public void testGetDisplayNameMetadata() {
    RelevancyAdminSession session = getRelevancyAdminSession();
    RelevancyForm createForm = getRelevancyFormForCreate(session);
    try {
      Metadata mdata = createForm.getDisplayNameMetadata();
    } catch (Exception e) {
      dump(e);
      fail("Caught unexpected exception:\n" + e.getMessage());
    }
    session.close();
  }

  // NOTICE: THIS METHOD IS GENERATED BY THE build_dxtera` REPOSITORY.
  //         ANY DIRECT CHANGES TO THIS CODE WILL BE OVERWRITTEN.
  //         TO REQUEST CHANGES, FILE A PR WITH AN EXAMPLE OF THE CHANGE
  //         YOU WOULD LIKE TO SEE.
  // Tests getDescriptionMetadata
  @Test
  public void testGetDescriptionMetadata() {
    RelevancyAdminSession session = getRelevancyAdminSession();
    RelevancyForm createForm = getRelevancyFormForCreate(session);
    try {
      Metadata mdata = createForm.getDescriptionMetadata();
    } catch (Exception e) {
      dump(e);
      fail("Caught unexpected exception:\n" + e.getMessage());
    }
    session.close();
  }

  // Tests getRelevancyFormRecord
  // NOTICE: THIS TEST IS GENERATED BY THE build_dxtera` REPOSITORY.
  //         ANY DIRECT CHANGES TO THIS CODE WILL BE OVERWRITTEN.
  //         TO REQUEST CHANGES, FILE A PR WITH AN EXAMPLE OF THE CHANGE
  //         YOU WOULD LIKE TO SEE.
  @Ignore("test not implemented")
  @Test
  public void testGetRelevancyFormRecord() {
    // template with resource.ResourceForm.get_resource_form_record
    // return type = org.osid.ontology.records.RelevancyFormRecord
  }
}
