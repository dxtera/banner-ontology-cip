package org.dxtera.sis.impls.banner.cip.ontology;

import static org.junit.Assert.*;
import static org.junit.Assume.assumeNoException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import net.okapia.osid.kilimanjaro.BootLoader;
import net.okapia.osid.kilimanjaro.OsidVersions;
import net.okapia.osid.primordium.id.BasicId;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.osid.*;
import org.osid.hierarchy.Hierarchy;
import org.osid.id.Id;
import org.osid.id.IdList;
import org.osid.ontology.*;

public class SubjectHierarchySessionTest {

  protected OntologyManager mgr;

  // baseMgr used for the Id sanity tests for Lookup sessions
  protected OntologyManager baseMgr;
  private static final List<Id> ROOT_SUBJECT_IDS =
      Arrays.asList(BasicId.valueOf("Subject:cip-hierarchy-root@CCSNH"));
  private static final List<Id> LEVEL_1_SUBJECT_IDS =
      Arrays.asList(
          BasicId.valueOf("Subject:cip.2010.01@CCSNH"),
          BasicId.valueOf("Subject:cip.2010.23@CCSNH"),
          BasicId.valueOf("Subject:cip.2010.49@CCSNH"),
          BasicId.valueOf("Subject:cip.2010.50@CCSNH"),
          BasicId.valueOf("Subject:cip.2010.52@CCSNH"),
          BasicId.valueOf("Subject:cip.2010.53@CCSNH"),
          BasicId.valueOf("Subject:cip.2010.60@CCSNH"));
  private static final Id PARENT_ID_1 = BasicId.valueOf("Subject:cip.2010.52@CCSNH");
  private static final List<Id> CHILD_SUBJECT_IDS_OF_PARENT_ID_1 =
      Arrays.asList(
          BasicId.valueOf("Subject:cip.2010.52.01@CCSNH"),
          BasicId.valueOf("Subject:cip.2010.52.02@CCSNH"));
  private static final Id CHILD_ID_1 = BasicId.valueOf("Subject:cip.2010.53.01@CCSNH");
  private static final List<Id> PARENT_SUBJECT_IDS_OF_CHILD_ID_1 =
      Arrays.asList(BasicId.valueOf("Subject:cip.2010.53@CCSNH"));
  private static final List<Id> CHILD_SUBJECT_IDS_OF_CHILD_ID_1 =
      Arrays.asList(
          BasicId.valueOf("Subject:cip.2010.53.0101@CCSNH"),
          BasicId.valueOf("Subject:cip.2010.53.0102@CCSNH"));

  // set up the OntologyManager
  @Before
  public void setUpBeforeClass() {
    try {
      BootLoader loader = new BootLoader();
      OsidRuntimeManager runtime = loader.getRuntimeManager("test");

      baseMgr =
          (OntologyManager)
              runtime.getManager(
                  OSID.ONTOLOGY,
                  "org.dxtera.sis.impls.banner.cip.ontology.BannerOntologyManager",
                  OsidVersions.V3_0_0.getVersion());
      mgr =
          (OntologyManager)
              runtime.getManager(
                  OSID.ONTOLOGY,
                  "org.dxtera.sis.impls.banner.cip.ontology.BannerOntologyManager",
                  OsidVersions.V3_0_0.getVersion());

    } catch (UnsupportedException e) {
      dump(e);
      assumeNoException("OntologyManager not supported", e);
    } catch (OperationFailedException e) {
      dump(e);
      fail("Caught OperationFailedException.");
    } catch (NotFoundException e) {
      dump(e);
      fail("Implementation string NotFoundException:\n" + e.getMessage());
    } catch (NullArgumentException e) {
      dump(e);
      fail("Caught NullArgumentException.");
    } catch (ConfigurationErrorException e) {
      dump(e);
      fail("Caught ConfigurationErrorException.");
    } catch (Exception e) {
      dump(e);
      fail("Caught unexpected exception:\n" + e.getMessage());
    }
  }

  private void dump(Throwable t) {
    while (t != null) {
      System.out.println(t.getClass() + ": " + t.getMessage());
      t.printStackTrace();
      t = t.getCause();
      if (t != null) {
        System.out.print("Caused by ");
      }
    }
  }

  @After
  public void tearDownAfterClass() {
    mgr.close();
    baseMgr.close();
  }

  private SubjectHierarchySession getSubjectHierarchySession() {
    SubjectHierarchySession session = null;
    try {
      session = mgr.getSubjectHierarchySession();
    } catch (OperationFailedException e) {
      dump(e);
      fail("Caught OperationFailedException.");
    } catch (UnimplementedException e) {
      dump(e);
      assumeNoException("SubjectHierarchySession not supported", e);
    } catch (Exception e) {
      dump(e);
      fail("Caught unexpected exception:\n" + e.getMessage());
    }
    return session;
  }

  // Tests getSubjectHierarchyId
  @Test
  public void testGetSubjectHierarchyId() {
    SubjectHierarchySession session = getSubjectHierarchySession();
    try {
      Id id = session.getSubjectHierarchyId();
      System.out.println(id);
      assertEquals("Hierarchy:1@CCSNH", id.toString());
    } catch (Exception e) {
      dump(e);
      fail("Caught unexpected exception:\n" + e.getMessage());
    }
    session.close();
  }

  // Tests getSubjectHierarchy
  @Test
  public void testGetSubjectHierarchy() {
    SubjectHierarchySession session = getSubjectHierarchySession();
    try {
      Hierarchy hierarchy = session.getSubjectHierarchy();
      System.out.println(hierarchy.getId());
      assertEquals("Hierarchy:1@CCSNH", hierarchy.getId().toString());
      assertEquals("Main Hierarchy for Ontology 1", hierarchy.getDisplayName().getText());
    } catch (Exception e) {
      dump(e);
      fail("Caught unexpected exception:\n" + e.getMessage());
    }
    session.close();
  }

  // Tests testCanLookupSubjectHierarchy
  @Test
  public void testCanLookupSubjectHierarchy() {
    SubjectHierarchySession session = getSubjectHierarchySession();
    try {
      boolean result = session.canAccessSubjectHierarchy();
      assertTrue(result);
    } catch (Exception e) {
      dump(e);
      fail("Caught unexpected exception:\n" + e.getMessage());
    }
    session.close();
  }

  // Tests useComparativeSubjectView
  @Test
  public void testUseComparativeSubjectView() {
    SubjectHierarchySession session = getSubjectHierarchySession();
    try {
      session.useComparativeSubjectView();
    } catch (Exception e) {
      dump(e);
      fail("Caught unexpected exception:\n" + e.getMessage());
    }
    session.close();
  }

  // Tests usePlenarySubjectView
  @Test
  public void testUsePlenarySubjectView() {
    SubjectHierarchySession session = getSubjectHierarchySession();
    try {
      session.usePlenarySubjectView();
    } catch (Exception e) {
      dump(e);
      fail("Caught unexpected exception:\n" + e.getMessage());
    }
    session.close();
  }

  // Tests getRootSubjectIds
  @Test
  public void testGetRootSubjectIds() {
    SubjectHierarchySession session = getSubjectHierarchySession();
    try {
      IdList subjectIdList = session.getRootSubjectIds();
      assertEquals(1, subjectIdList.available());
      while (subjectIdList.hasNext()) {
        assertTrue(ROOT_SUBJECT_IDS.contains(subjectIdList.getNextId()));
      }
    } catch (NullArgumentException e) {
      dump(e);
      fail("Caught NullArgumentException.");
    } catch (OperationFailedException e) {
      dump(e);
      fail("Caught OperationFailedException.");
    } catch (PermissionDeniedException e) {
      dump(e);
      assumeNoException("Caught PermissionDeniedException", e);
    } catch (Exception e) {
      dump(e);
      fail("Caught unexpected exception:\n" + e.getMessage());
    }
    session.close();
  }

  // Tests getRootSubjects
  @Test
  public void testGetRootSubjects() {
    SubjectHierarchySession session = getSubjectHierarchySession();
    try {
      SubjectList subjects = session.getRootSubjects();
      assertEquals(1, subjects.available());
      while (subjects.hasNext()) {
        Subject subject = subjects.getNextSubject();
        assertTrue(ROOT_SUBJECT_IDS.contains(subject.getId()));
        assertEquals("CIP Hierarchy Root", subject.getDescription().getText());
      }
    } catch (NullArgumentException e) {
      dump(e);
      fail("Caught NullArgumentException.");
    } catch (OperationFailedException e) {
      dump(e);
      fail("Caught OperationFailedException.");
    } catch (PermissionDeniedException e) {
      dump(e);
      assumeNoException("Caught PermissionDeniedException", e);
    } catch (Exception e) {
      dump(e);
      fail("Caught unexpected exception:\n" + e.getMessage());
    }
    session.close();
  }

  // Tests getChildSubjectIds
  @Test
  public void testGetChildSubjectIds() {
    SubjectHierarchySession session = getSubjectHierarchySession();
    try {
      IdList childSubjectIds = session.getChildSubjectIds(PARENT_ID_1);
      assertEquals(2, childSubjectIds.available());
      while (childSubjectIds.hasNext()) {
        assertTrue(CHILD_SUBJECT_IDS_OF_PARENT_ID_1.contains(childSubjectIds.getNextId()));
      }
      // get child for root
      childSubjectIds = session.getChildSubjectIds(ROOT_SUBJECT_IDS.get(0));
      assertEquals(7, childSubjectIds.available());
      while (childSubjectIds.hasNext()) {
        assertTrue(LEVEL_1_SUBJECT_IDS.contains(childSubjectIds.getNextId()));
      }

    } catch (NullArgumentException e) {
      dump(e);
      fail("Caught NullArgumentException.");
    } catch (OperationFailedException e) {
      dump(e);
      fail("Caught OperationFailedException.");
    } catch (PermissionDeniedException e) {
      dump(e);
      assumeNoException("Caught PermissionDeniedException", e);
    } catch (Exception e) {
      dump(e);
      fail("Caught unexpected exception:\n" + e.getMessage());
    }

    session.close();
  }

  // Tests getChildSubjects
  @Test
  public void testGetChildSubjects() {
    SubjectHierarchySession session = getSubjectHierarchySession();
    try {
      SubjectList childSubjects = session.getChildSubjects(PARENT_ID_1);
      assertEquals(2, childSubjects.available());
      while (childSubjects.hasNext()) {
        assertTrue(
            CHILD_SUBJECT_IDS_OF_PARENT_ID_1.contains(childSubjects.getNextSubject().getId()));
      }
      // get child for root
      childSubjects = session.getChildSubjects(ROOT_SUBJECT_IDS.get(0));
      assertEquals(7, childSubjects.available());
      while (childSubjects.hasNext()) {
        assertTrue(LEVEL_1_SUBJECT_IDS.contains(childSubjects.getNextSubject().getId()));
      }
    } catch (NullArgumentException e) {
      dump(e);
      fail("Caught NullArgumentException.");
    } catch (OperationFailedException e) {
      dump(e);
      fail("Caught OperationFailedException.");
    } catch (PermissionDeniedException e) {
      dump(e);
      assumeNoException("Caught PermissionDeniedException", e);
    } catch (Exception e) {
      dump(e);
      fail("Caught unexpected exception:\n" + e.getMessage());
    }

    session.close();
  }

  // Tests getParentSubjectIds
  @Test
  public void testGetParentSubjectIds() {
    SubjectHierarchySession session = getSubjectHierarchySession();
    try {
      IdList parentSubjectIds = session.getParentSubjectIds(CHILD_ID_1);
      assertEquals(1, parentSubjectIds.available());
      while (parentSubjectIds.hasNext()) {
        assertTrue(PARENT_SUBJECT_IDS_OF_CHILD_ID_1.contains(parentSubjectIds.getNextId()));
      }

      // get parent for root
      parentSubjectIds = session.getParentSubjectIds(ROOT_SUBJECT_IDS.get(0));
      assertEquals(0, parentSubjectIds.available());

      // get parent for level 1 ids
      for (Id level1SubjectId : LEVEL_1_SUBJECT_IDS) {
        parentSubjectIds = session.getParentSubjectIds(level1SubjectId);
        assertEquals(1, parentSubjectIds.available());
        while (parentSubjectIds.hasNext()) {
          assertTrue(ROOT_SUBJECT_IDS.contains(parentSubjectIds.getNextId()));
        }
      }

    } catch (NullArgumentException e) {
      dump(e);
      fail("Caught NullArgumentException.");
    } catch (OperationFailedException e) {
      dump(e);
      fail("Caught OperationFailedException.");
    } catch (PermissionDeniedException e) {
      dump(e);
      assumeNoException("Caught PermissionDeniedException", e);
    } catch (Exception e) {
      dump(e);
      fail("Caught unexpected exception:\n" + e.getMessage());
    }

    session.close();
  }

  // Tests getParentSubjects
  @Test
  public void testGetParentSubjects() {
    SubjectHierarchySession session = getSubjectHierarchySession();
    try {
      SubjectList parentSubjects = session.getParentSubjects(CHILD_ID_1);
      assertEquals(1, parentSubjects.available());
      while (parentSubjects.hasNext()) {
        assertTrue(
            PARENT_SUBJECT_IDS_OF_CHILD_ID_1.contains(parentSubjects.getNextSubject().getId()));
      }

      // get parent for root
      parentSubjects = session.getParentSubjects(ROOT_SUBJECT_IDS.get(0));
      assertEquals(0, parentSubjects.available());

      // get parent for level 1 ids
      for (Id level1SubjectId : LEVEL_1_SUBJECT_IDS) {
        parentSubjects = session.getParentSubjects(level1SubjectId);
        assertEquals(1, parentSubjects.available());
        while (parentSubjects.hasNext()) {
          assertTrue(ROOT_SUBJECT_IDS.contains(parentSubjects.getNextSubject().getId()));
        }
      }
    } catch (NullArgumentException e) {
      dump(e);
      fail("Caught NullArgumentException.");
    } catch (OperationFailedException e) {
      dump(e);
      fail("Caught OperationFailedException.");
    } catch (PermissionDeniedException e) {
      dump(e);
      assumeNoException("Caught PermissionDeniedException", e);
    } catch (Exception e) {
      dump(e);
      fail("Caught unexpected exception:\n" + e.getMessage());
    }

    session.close();
  }

  // Tests getSubjectNodes
  @Test
  public void testGetSubjectNodes() {
    SubjectHierarchySession session = getSubjectHierarchySession();
    try {
      SubjectNode subjectNodes = session.getSubjectNodes(CHILD_ID_1, 1L, 1L, true);
      // check child subject nodes
      SubjectNodeList childSubjectNodes = subjectNodes.getChildSubjectNodes();
      assertEquals(2, childSubjectNodes.available());
      while (childSubjectNodes.hasNext()) {
        assertTrue(
            CHILD_SUBJECT_IDS_OF_CHILD_ID_1.contains(
                childSubjectNodes.getNextSubjectNode().getSubject().getId()));
      }
      // check parent subject nodes
      SubjectNodeList parentSubjectNodes = subjectNodes.getParentSubjectNodes();
      assertEquals(1, parentSubjectNodes.available());
      while (parentSubjectNodes.hasNext()) {
        assertTrue(
            PARENT_SUBJECT_IDS_OF_CHILD_ID_1.contains(
                parentSubjectNodes.getNextSubjectNode().getSubject().getId()));
      }
      // check child ids
      IdList childIds = subjectNodes.getChildIds();
      assertEquals(2, childIds.available());
      while (childIds.hasNext()) {
        assertTrue(CHILD_SUBJECT_IDS_OF_CHILD_ID_1.contains(childIds.getNextId()));
      }
      // check parent subject nodes
      IdList parentIds = subjectNodes.getParentIds();
      assertEquals(1, parentIds.available());
      while (childIds.hasNext()) {
        assertTrue(PARENT_SUBJECT_IDS_OF_CHILD_ID_1.contains(parentIds.getNextId()));
      }
      // check subject node id
      assertEquals(CHILD_ID_1, subjectNodes.getId());
      // check subject id
      assertEquals(CHILD_ID_1, subjectNodes.getSubject().getId());
    } catch (NullArgumentException e) {
      dump(e);
      fail("Caught NullArgumentException.");
    } catch (OperationFailedException e) {
      dump(e);
      fail("Caught OperationFailedException.");
    } catch (PermissionDeniedException e) {
      dump(e);
      assumeNoException("Caught PermissionDeniedException", e);
    } catch (Exception e) {
      dump(e);
      fail("Caught unexpected exception:\n" + e.getMessage());
    }

    session.close();
  }

  @Test
  @Ignore(
      "This test needs to be run for original cip file in order to check the subjects and hierarchy")
  public void testGetParentAndChildSubjects() {
    SubjectHierarchySession session = getSubjectHierarchySession();
    try {
      long allSubjectsCount = 0;
      SubjectList subjects = mgr.getSubjectLookupSession().getSubjects();
      allSubjectsCount = subjects.available();
      List<Subject> allSubjectList = new ArrayList<>();
      while (subjects.hasNext()) {
        Subject subject = subjects.getNextSubject();
        if (!allSubjectList.contains(subject)) {
          allSubjectList.add(subject);
        }
      }
      System.out.println("All Subjects: " + allSubjectList.size());
      assertEquals(2318, allSubjectsCount);

      List<Subject> subjectListFromHierarchies = new ArrayList<>();
      long allSubjectsInHierarchyCount = 0;
      SubjectList rootSubjects = session.getRootSubjects();
      allSubjectsInHierarchyCount += rootSubjects.available();
      while (rootSubjects.hasNext()) {
        Subject subject = rootSubjects.getNextSubject();
        subjectListFromHierarchies.add(subject);

        // add children of root to all subjects in hierarchy
        SubjectList childSubjects = session.getChildSubjects(subject.getId());
        allSubjectsInHierarchyCount += childSubjects.available();
        while (childSubjects.hasNext()) {
          Subject child = childSubjects.getNextSubject();
          subjectListFromHierarchies.add(child);
          System.out.println("Child Subject Identifier: " + child.getId().getIdentifier());
        }
      }

      for (Subject subject : allSubjectList) {
        System.out.println("Subject Identifier: " + subject.getId().getIdentifier());
        SubjectList childSubjects = session.getChildSubjects(subject.getId());
        allSubjectsInHierarchyCount += childSubjects.available();
        while (childSubjects.hasNext()) {
          Subject child = childSubjects.getNextSubject();
          subjectListFromHierarchies.add(child);
          System.out.println("Child Subject Identifier: " + child.getId().getIdentifier());
        }
      }
      assertEquals(2319, subjectListFromHierarchies.size());

      System.out.println("Subjects in Hierarchy: " + allSubjectsInHierarchyCount);
      assertTrue(allSubjectsCount <= allSubjectsInHierarchyCount);

      System.out.println("Subjects not present in hierarchy: ");
      long subjectsNotInHierarchy =
          allSubjectList
              .stream()
              .filter(subject -> !subjectListFromHierarchies.contains(subject))
              .count();
      assertEquals(0, subjectsNotInHierarchy);
      System.out.println(subjectsNotInHierarchy);
    } catch (NullArgumentException e) {
      dump(e);
      fail("Caught NullArgumentException.");
    } catch (OperationFailedException e) {
      dump(e);
      fail("Caught OperationFailedException.");
    } catch (PermissionDeniedException e) {
      dump(e);
      assumeNoException("Caught PermissionDeniedException", e);
    } catch (Exception e) {
      dump(e);
      fail("Caught unexpected exception:\n" + e.getMessage());
    }

    session.close();
  }
}
