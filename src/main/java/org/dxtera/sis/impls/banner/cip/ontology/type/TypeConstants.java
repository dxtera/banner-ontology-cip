package org.dxtera.sis.impls.banner.cip.ontology.type;

import static net.okapia.osid.primordium.locale.text.eng.us.Plain.text;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import net.okapia.osid.primordium.calendaring.GregorianUTCDateTime;
import net.okapia.osid.primordium.types.calendar.CalendarTypes;
import net.okapia.osid.primordium.types.text.FormatTypes;
import net.okapia.osid.primordium.types.text.ISOMajorLanguageTypes;
import net.okapia.osid.primordium.types.text.ISOScriptTypes;
import net.okapia.osid.primordium.types.time.EarthTimeTypes;
import org.osid.calendaring.DateTime;
import org.osid.type.Type;

/**
 * Constants
 *
 * @author nwright
 */
public class TypeConstants {

  public static final Type PLAIN_TEXT_FORMAT = FormatTypes.PLAIN.getType();
  public static final Type ENGLISH_LANGUAGE = ISOMajorLanguageTypes.ENG.getType();
  public static final Type LATIN_SCRIPT = ISOScriptTypes.LATN.getType();

  public static final Type GREGORIAN_CALENDAR = CalendarTypes.GREGORIAN.getType();
  public static final Type UNKNOWN_CALENDAR;
  public static final Type UTC_TIME = EarthTimeTypes.UTC.getType();
  public static final Type UNKNOWN_TIME;

  public static final DateFormat YYYY_MM_DD = new SimpleDateFormat("yyyy-MM-dd");
  public static TypeBean bean;
  public static final Type CIP2010_SUBJECT;
  public static final String PROGRAM2CIP_RELEVANCY_IDENTIFIER = "program.2.cip.2010";
  public static final Type PROGRAM2CIP_RELEVANCY;

  public static Date parseYYYY_MM_DD(String date) {
    try {
      return YYYY_MM_DD.parse(date);
    } catch (ParseException ex) {
      throw new IllegalArgumentException(ex);
    }
  }

  public static final DateTime BEGINNING_OF_TIME =
      GregorianUTCDateTime.valueOf(parseYYYY_MM_DD("1900-01-01"));
  public static final DateTime END_OF_TIME =
      GregorianUTCDateTime.valueOf(parseYYYY_MM_DD("9999-12-31"));

  static {
    TypeBean bean;
    bean = new TypeBean();
    bean.setAuthority("dxtera");
    bean.setIdentifierNamespace(IdNamespaceConstants.CALENDAR);
    bean.setIdentifier("unknown");
    bean.setDisplayLabel(text("Unknown"));
    bean.setDisplayName(text("Unknoiwn Calendar"));
    bean.setDescription(text("Unknown Calendar -- used to indicate this DateTime is not known"));
    bean.setDomain(text(bean.getIdentifierNamespace()));
    UNKNOWN_CALENDAR = bean;

    bean = new TypeBean();
    bean.setAuthority("dxtera");
    bean.setIdentifierNamespace(IdNamespaceConstants.TIME);
    bean.setIdentifier("unknown");
    bean.setDisplayLabel(text("Unknown "));
    bean.setDisplayName(text("Unknown Time"));
    bean.setDescription(text("Unknown Time"));
    bean.setDomain(text(bean.getIdentifierNamespace()));
    UNKNOWN_TIME = bean;

    bean = new TypeBean();
    bean.setAuthority("dxtera");
    bean.setIdentifierNamespace(IdNamespaceConstants.SUBJECT);
    bean.setIdentifier("cip.2010");
    bean.setDisplayLabel(text("CIP2010 Subject"));
    bean.setDisplayName(text("CIP2010 Subject"));
    bean.setDescription(text("CIP2010 Subject"));
    bean.setDomain(text(bean.getIdentifierNamespace()));
    CIP2010_SUBJECT = bean;

    bean = new TypeBean();
    bean.setAuthority("dxtera");
    bean.setIdentifierNamespace(IdNamespaceConstants.RELEVANCY);
    bean.setIdentifier(PROGRAM2CIP_RELEVANCY_IDENTIFIER);
    bean.setDisplayLabel(text("Program to CIP 2010 Relevancy"));
    bean.setDisplayName(text("Program to CIP 2010 Relevancy"));
    bean.setDescription(text("Program to CIP 2010 Relevancy"));
    bean.setDomain(text(bean.getIdentifierNamespace()));
    PROGRAM2CIP_RELEVANCY = bean;
  }

  public static final Type DEFAULT_ONTOLOGY;
  public static final Type RELEVANCY_BRANCH;
  public static final Type SUBJECT_BRANCH;
  public static final Type RELEVANCY_JOURNAL;
  public static final Type SUBJECT_JOURNAL;
  public static final Type RELEVANCY_JOURNAL_ENTRY;
  public static final Type SUBJECT_JOURNAL_ENTRY;

  static {
    bean = new TypeBean();
    bean.setAuthority("dxtera");
    bean.setIdentifierNamespace(IdNamespaceConstants.ONTOLOGY);
    bean.setIdentifier("RelevancyBranch");
    bean.setDisplayLabel(text("RelevancyBranch"));
    bean.setDisplayName(text("Relevancy Branch"));
    bean.setDescription(text("Default Branch for Relevancies"));
    bean.setDomain(text(bean.getIdentifierNamespace()));
    DEFAULT_ONTOLOGY = (Type) bean;

    bean = new TypeBean();
    bean.setAuthority("dxtera");
    bean.setIdentifierNamespace(IdNamespaceConstants.BRANCH);
    bean.setIdentifier("RelevancyBranch");
    bean.setDisplayLabel(text("RelevancyBranch"));
    bean.setDisplayName(text("Relevancy Branch"));
    bean.setDescription(text("Default Branch for Relevancies"));
    bean.setDomain(text(bean.getIdentifierNamespace()));
    RELEVANCY_BRANCH = (Type) bean;

    bean = new TypeBean();
    bean.setAuthority("dxtera");
    bean.setIdentifierNamespace(IdNamespaceConstants.BRANCH);
    bean.setIdentifier("SubjectBranch");
    bean.setDisplayLabel(text("SubjectBranch"));
    bean.setDisplayName(text("Subject Branch"));
    bean.setDescription(text("Default Branch for Subjectes"));
    bean.setDomain(text(bean.getIdentifierNamespace()));
    SUBJECT_BRANCH = (Type) bean;

    bean = new TypeBean();
    bean.setAuthority("dxtera");
    bean.setIdentifierNamespace(IdNamespaceConstants.JOURNAL);
    bean.setIdentifier("RelevancyJournal");
    bean.setDisplayLabel(text("RelevancyJournal"));
    bean.setDisplayName(text("Relevancy Journal"));
    bean.setDescription(text("Default Journal for Relevancys"));
    bean.setDomain(text(bean.getIdentifierNamespace()));
    RELEVANCY_JOURNAL = (Type) bean;

    bean = new TypeBean();
    bean.setAuthority("dxtera");
    bean.setIdentifierNamespace(IdNamespaceConstants.JOURNAL);
    bean.setIdentifier("SubjectJournal");
    bean.setDisplayLabel(text("SubjectJournal"));
    bean.setDisplayName(text("Subject Journal"));
    bean.setDescription(text("Default Journal for Subjectes"));
    bean.setDomain(text(bean.getIdentifierNamespace()));
    SUBJECT_JOURNAL = (Type) bean;

    bean = new TypeBean();
    bean.setAuthority("dxtera");
    bean.setIdentifierNamespace(IdNamespaceConstants.JOURNAL_ENTRY);
    bean.setIdentifier("RelevancyJournalEntry");
    bean.setDisplayLabel(text("RelevancyJournalEntry"));
    bean.setDisplayName(text("Relevancy JournalEntry"));
    bean.setDescription(text("Default JournalEntry for Relevancys"));
    bean.setDomain(text(bean.getIdentifierNamespace()));
    RELEVANCY_JOURNAL_ENTRY = (Type) bean;

    bean = new TypeBean();
    bean.setAuthority("dxtera");
    bean.setIdentifierNamespace(IdNamespaceConstants.JOURNAL_ENTRY);
    bean.setIdentifier("SubjectJournalEntry");
    bean.setDisplayLabel(text("SubjectJournalEntry"));
    bean.setDisplayName(text("Subject JournalEntry"));
    bean.setDescription(text("Default JournalEntry for Subjectes"));
    bean.setDomain(text(bean.getIdentifierNamespace()));
    SUBJECT_JOURNAL_ENTRY = (Type) bean;
  }

  public static final Type HIERARCHY;

  static {
    TypeBean bean = new TypeBean();
    bean.setAuthority("dxtera");
    bean.setIdentifierNamespace(IdNamespaceConstants.HIERARCHY);
    bean.setIdentifier("hierarchy");
    bean.setDisplayLabel(text("Hierarchy"));
    bean.setDisplayName(text("Hierarchy"));
    bean.setDescription(text("Hierarchy"));
    bean.setDomain(text(bean.getIdentifierNamespace()));
    HIERARCHY = bean;
  }
}
