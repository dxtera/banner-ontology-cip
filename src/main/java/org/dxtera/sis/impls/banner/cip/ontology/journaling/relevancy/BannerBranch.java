package org.dxtera.sis.impls.banner.cip.ontology.journaling.relevancy;

import static net.okapia.osid.primordium.locale.text.eng.us.Plain.text;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.InputStream;
import java.sql.ResultSet;
import java.util.HashMap;
import net.okapia.osid.jamocha.builder.journaling.journalentry.MutableJournalEntry;
import net.okapia.osid.primordium.id.BasicId;
import net.okapia.osid.primordium.type.BasicType;
import org.dxtera.sis.impls.banner.cip.ontology.AuthorityInterface;
import org.dxtera.sis.impls.banner.cip.ontology.journaling.BranchGeneratorInterface;
import org.dxtera.utils.database.postgres.ResultSetUtils;
import org.osid.OperationFailedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// This class is the ``Branch`` entity. Until we
//   find out otherwise, we'll use a single Branch
//   for all the journaling implementations.
// If we find out that there are Branch variants,
//   then we may need to create implementation-specific
//   Branch classes.

// Scott Thorne, Cole Shaw, Amon Horne
// Okapia
// February 2017
//
// Copyright (c) 2017 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicense, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// We want to extend from jamocha when we can, since it provides
// a lot of built-in functionality.
// We always want to state which OSID interfaces we are implementing.
// We also implement the Generator interface so that
// we can use the protected setters from
// jamocha.
public class BannerBranch extends net.okapia.osid.jamocha.journaling.branch.spi.AbstractBranch
    implements org.osid.journaling.Branch, BranchGeneratorInterface, AuthorityInterface {

  protected String authority;

  public String getAuthority() {

    return this.authority;
  }

  public void setAuthority(String authority) {
    this.authority = authority;
  }

  public BannerBranch(String authority) throws OperationFailedException {
    super();
    this.authority = authority;
  }

  public BannerBranch() throws OperationFailedException {
    super();
    this.authority = "dxtera";
  }

  public HashMap<String, String> getMapFile(String fileName) {
    StringBuilder filePath = new StringBuilder();
    String basePath = "cip/map/journaling/" + fileName;
    String extPath = "cip/map/" + authority + "/journaling/" + fileName;
    HashMap<String, String> jsonMap = new HashMap<>();
    try {
      ObjectMapper objectMapper = new ObjectMapper();
      InputStream input =
          (Thread.currentThread().getContextClassLoader().getResourceAsStream(extPath.toString())
                  != null)
              ? Thread.currentThread()
                  .getContextClassLoader()
                  .getResourceAsStream(extPath.toString())
              : Thread.currentThread()
                  .getContextClassLoader()
                  .getResourceAsStream(basePath.toString());
      jsonMap = objectMapper.readValue(input, new TypeReference<HashMap<String, String>>() {});
    } catch (Exception e) {
      e.printStackTrace();
    }
    return jsonMap;
  }

  // This method takes an SQL ResultSet and converts
  //   it into a Branch object.
  @Override
  public org.osid.journaling.Branch convert(ResultSet rs) throws OperationFailedException {

    HashMap<String, String> typeMap = this.getMapFile("branchfile.json");
    Logger logger = LoggerFactory.getLogger(BannerBranch.class);

    current();

    // Note that we use our package's Branch object
    //   so that we can access the setter methods.
    BannerBranch branch = new BannerBranch(this.authority);

    // You can set values using the setters for our impl
    //   and pull values from the ResultSet.
    // Here is where you would modify the column names to correctly map to
    //   the Banner implementation.
    try {
      branch.setId(
          new net.okapia.osid.primordium.id.BasicId(this.authority, "Branch", rs.getString("id")));
      branch.setDisplayName(text(ResultSetUtils.getString(rs.getString("display_name"))));
      branch.setDescription(text(ResultSetUtils.getString(rs.getString("description"))));

      if (typeMap.get(rs.getString("pt_id")) == null) {
        logger.info("ERR - Missing Branch Type for: " + rs.getString("pt_id"));
      }
      branch.setGenusType(
          new BasicType(
              this.authority,
              "Branch",
              typeMap.getOrDefault(rs.getString("pt_id"), rs.getString("pt_id"))));

      // If Banner requires special handling of any field types, you can create
      //   a Utility class that differs from our PostgreSQL version.

      MutableJournalEntry originJournalEntry = new MutableJournalEntry();
      originJournalEntry.setId(BasicId.valueOf(rs.getString("origin_journal_entry")));
      branch.setOriginJournalEntry(originJournalEntry);

      MutableJournalEntry latestJournalEntry = new MutableJournalEntry();
      latestJournalEntry.setId(BasicId.valueOf(rs.getString("latest_journal_entry")));
      branch.setLatestJournalEntry(latestJournalEntry);

      // Once we support record types, you should also check
      //   and call addRecord() here.

      // if (hasRecordType(BasicType.valueOf(rs.getString("genus_type")))) {
      //     addRecord(getTermRecord(BasicType.valueOf(rs.getString("genus_type"))));
      // }
    } catch (java.sql.SQLException e) {
      // Do something here
    }

    return branch;
  }

  /**
   * This method copies an "unknown" provider Branch into a Branch object from this package. This is
   * often helpful to ensure that a Branch behaves as expected.
   */
  public org.osid.journaling.Branch convert(org.osid.journaling.Branch other)
      throws OperationFailedException {

    // Note that we use our package's Branch object
    //   so that we can access the setter methods.
    BannerBranch branch = new BannerBranch(this.authority);

    branch.setId(other.getId());
    branch.setDescription(other.getDescription());
    branch.setDisplayName(other.getDisplayName());
    branch.setGenusType(other.getGenusType());

    MutableJournalEntry originJournalEntry = new MutableJournalEntry();
    originJournalEntry.setId(other.getOriginJournalEntryId());
    branch.setOriginJournalEntry(originJournalEntry);

    MutableJournalEntry latestJournalEntry = new MutableJournalEntry();
    latestJournalEntry.setId(other.getLatestJournalEntryId());
    branch.setLatestJournalEntry(latestJournalEntry);

    // Once we support record types, you should also check
    //   and call addRecord() here.

    // if (hasRecordType(other.getGenusType())) {
    //     addRecord(other.getTermRecord(other.getGenusType()));
    // }

    return branch;
  }

  // If you need to, you can override the default Jamocha
  // behavior for this method.
  @Override
  public boolean isActive() {
    return super.isActive();
  }

  // If you need to, you can override the default Jamocha
  // behavior for this method.
  @Override
  public boolean isEnabled() {
    return super.isEnabled();
  }

  // If you need to, you can override the default Jamocha
  // behavior for this method.
  @Override
  public boolean isDisabled() {
    return super.isDisabled();
  }

  // If you need to, you can override the default Jamocha
  // behavior for this method.
  @Override
  public boolean isOperational() {
    return super.isOperational();
  }
}
