package org.dxtera.sis.impls.banner.cip.ontology;

import java.sql.ResultSet;
import org.dxtera.utils.iterators.core.Converter;

// Scott Thorne
// Okapia
// February 2017
//
// Copyright (c) 2017 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicense, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

// We extend the SIS Impls Converter here
// because we will be using the SIS Impls
// ResultSetIteratorWithTotal. The Jamocha
// JDBC List wrappers seem to hang when
// the buffer size is not large enough.
// See FRAM-67.
public interface OntologyGeneratorInterface
    extends Converter<ResultSet, org.osid.ontology.Ontology> {

  /** This method takes an SQL ResultSet and converts it into a Ontology object. */
  @Override
  public org.osid.ontology.Ontology convert(ResultSet rs) throws org.osid.OperationFailedException;

  /**
   * This method copies an "unknown" provider Ontology into a Ontology object from this package.
   * This is often helpful to ensure that a Ontology behaves as expected.
   */
  public org.osid.ontology.Ontology convert(org.osid.ontology.Ontology a)
      throws org.osid.OperationFailedException;
}
