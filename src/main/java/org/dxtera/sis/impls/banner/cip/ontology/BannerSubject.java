package org.dxtera.sis.impls.banner.cip.ontology;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.InputStream;
import java.util.HashMap;
import net.okapia.osid.jamocha.builder.ontology.subject.MutableSubject;
import net.okapia.osid.primordium.id.BasicId;
import net.okapia.osid.primordium.locale.text.eng.us.Plain;
import org.dxtera.sis.impls.banner.cip.ontology.type.IdNamespaceConstants;
import org.dxtera.sis.impls.banner.cip.ontology.type.TypeConstants;
import org.dxtera.sis.impls.banner.csv.beans.CipBean;
import org.osid.OperationFailedException;
import org.osid.type.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// This class is the ``Subject`` entity. Until we
//   find out otherwise, we'll use a single Subject
//   for all the ontology implementations.
// If we find out that there are Subject variants,
//   then we may need to create implementation-specific
//   Subject classes.

// Scott Thorne, Cole Shaw, Amon Horne
// Okapia
// February 2017
//
// Copyright (c) 2017 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicense, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// We want to extend from jamocha when we can, since it provides
// a lot of built-in functionality.
// We always want to state which OSID interfaces we are implementing.
// We also implement the Generator interface so that
// we can use the protected setters from
// jamocha.
public class BannerSubject extends net.okapia.osid.jamocha.ontology.subject.spi.AbstractSubject
    implements org.osid.ontology.Subject, SubjectGeneratorInterface, AuthorityInterface {

  private final Logger logger = LoggerFactory.getLogger(BannerSubject.class.getName());

  protected String authority;

  public String getAuthority() {

    return this.authority;
  }

  public void setAuthority(String authority) {
    this.authority = authority;
  }

  public BannerSubject(String authority) throws OperationFailedException {
    super();
    this.authority = authority;
  }

  public BannerSubject() throws OperationFailedException {
    super();
    this.authority = "dxtera";
  }

  public HashMap<String, String> getMapFile(String fileName) {
    StringBuilder filePath = new StringBuilder();
    String basePath = "cip/map/ontology/" + fileName;
    String extPath = "cip/map/" + authority + "/ontology/" + fileName;
    HashMap<String, String> jsonMap = new HashMap<>();
    try {
      ObjectMapper objectMapper = new ObjectMapper();
      InputStream input =
          (Thread.currentThread().getContextClassLoader().getResourceAsStream(extPath.toString())
                  != null)
              ? Thread.currentThread()
                  .getContextClassLoader()
                  .getResourceAsStream(extPath.toString())
              : Thread.currentThread()
                  .getContextClassLoader()
                  .getResourceAsStream(basePath.toString());
      jsonMap = objectMapper.readValue(input, new TypeReference<HashMap<String, String>>() {});
    } catch (Exception e) {
      e.printStackTrace();
    }
    return jsonMap;
  }

  // This method takes the CipBean and converts
  //   it into a Subject object.
  @Override
  public org.osid.ontology.Subject convert(CipBean cipBean) throws OperationFailedException {

    MutableSubject mutableSubject = new MutableSubject();
    mutableSubject.setId(
        new BasicId(
            this.authority,
            IdNamespaceConstants.SUBJECT,
            IdNamespaceConstants.CIP_ID_PREFIX + cipBean.getCipCode()));
    mutableSubject.setGenusType(toGenusType(cipBean));
    mutableSubject.setDisplayName(new Plain(this.calcName(cipBean)));
    mutableSubject.setDescription(new Plain(this.calcDescription(cipBean)));
    return mutableSubject;
  }

  /**
   * This method copies an "unknown" provider Subject into a Subject object from this package. This
   * is often helpful to ensure that a Subject behaves as expected.
   */
  public org.osid.ontology.Subject convert(org.osid.ontology.Subject other)
      throws OperationFailedException {

    // Note that we use our package's Subject object
    //   so that we can access the setter methods.
    BannerSubject subject = new BannerSubject(this.authority);

    subject.setId(other.getId());
    subject.setDescription(other.getDescription());
    subject.setDisplayName(other.getDisplayName());
    subject.setGenusType(other.getGenusType());

    // Once we support record types, you should also check
    //   and call addRecord() here.

    // if (hasRecordType(other.getGenusType())) {
    //     addRecord(other.getTermRecord(other.getGenusType()));
    // }

    return subject;
  }

  protected String calcName(CipBean deptEd) {
    StringBuilder sb = new StringBuilder();
    sb.append(deptEd.getCipCode());
    sb.append(" -- ");
    sb.append(deptEd.getCipTitle());
    return sb.toString();
  }

  protected String calcDescription(CipBean deptEd) {
    StringBuilder sb = new StringBuilder();
    sb.append(deptEd.getCipDefinition());
    sb.append("\n");
    sb.append("\n");
    sb.append("\nCrosssreferences: ");
    sb.append(deptEd.getCipCrossreferences());
    sb.append("\n");
    sb.append("\n");
    sb.append("\nExamples: ");
    sb.append(deptEd.getExamples());
    return sb.toString();
  }

  /**
   * Intended to be overridden by a institution
   *
   * @param subject
   * @return The genus type, Fall, Spring, etc...
   */
  protected Type toGenusType(CipBean subject) {
    return TypeConstants.CIP2010_SUBJECT;
  }
}
