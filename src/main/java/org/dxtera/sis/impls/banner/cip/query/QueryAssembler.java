package org.dxtera.sis.impls.banner.cip.query;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.naming.InitialContext;
import org.dxtera.utils.iterators.core.ResultSetIteratorWithTotal;
import org.osid.OperationFailedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.okapia.osid.primordium.id.BasicId;
import java.util.Iterator;
import java.util.HashSet;


public class QueryAssembler {
  List<QueryElement> queryTerms;
  Integer termCount;
  QueryElement qe;
  StringBuilder bq;
  String queryId;
  String queryClassName;
  ResultSetIteratorWithTotal rsIt;
  String sqlpath;
  InitialContext initialContext;
  Logger logger;
  String authority;

  public QueryAssembler(String queryId, String sqlpath) throws OperationFailedException {
    this.queryId = queryId;
    logger = LoggerFactory.getLogger(QueryAssembler.class.getName());
    org.osid.id.Id param1 = BasicId.valueOf("configuration:institution@local");
    this.authority = sqlpath;
  }

  public String getBaseQuery(String packageName) {
    String basePath = "cip/sqldir/" + packageName + "/" + queryId + ".sql";
    String extPath = "cip/sqldir/" + this.authority + "/" + packageName + "/" + queryId + ".sql";
    bq = new StringBuilder();
    String s;
    try {
      InputStream input =
          (Thread.currentThread().getContextClassLoader().getResourceAsStream(extPath.toString())
                  != null)
              ? Thread.currentThread()
                  .getContextClassLoader()
                  .getResourceAsStream(extPath.toString())
              : Thread.currentThread()
                  .getContextClassLoader()
                  .getResourceAsStream(basePath.toString());
      BufferedReader reader = new BufferedReader(new InputStreamReader(input));
      while ((s = reader.readLine()) != null) {
        bq.append(s);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return bq.toString();
  }

  public ResultSetIteratorWithTotal processQuery(
      Connection connect, org.osid.OsidObjectQuery query, String packageName)
      throws org.osid.OperationFailedException {
    QueryTermInterface qti;
    try {
      qti = (QueryTermInterface) query;
    } catch (Exception e) {
      throw new org.osid.OperationFailedException("Unable to cast to QueryTermInterface.");
    }
    logger.debug("HERE?");
    queryTerms = qti.getQueryTerms();
    logger.debug("Size of QT: " + queryTerms.size());

    java.util.HashSet<String> idl = new java.util.HashSet<String>();
    java.util.List<QueryElement> qq = new java.util.ArrayList<QueryElement>();
    Iterator<QueryElement> q1 =queryTerms.iterator();
    while(q1.hasNext()) {
      QueryElement qa = q1.next();
        if (qa.getType().equals("id")) {
                idl.add(qa.getId());
                qq.add(qa);
        }
    }
    Iterator<QueryElement> q2 = qq.iterator();
    while(q2.hasNext())
    {
            queryTerms.remove(q2.next());
    }


    termCount = queryTerms.size();
    java.util.ArrayList<String> parmArray = new java.util.ArrayList<>();
    java.util.ArrayList<String> parmTypeArray = new java.util.ArrayList<>();
    java.util.HashSet<KeyElement> keySet = new java.util.HashSet<KeyElement>();
    getBaseQuery(packageName);
    logger.info("Query Base: " + bq);
    StringBuilder where = new StringBuilder();
    ResultSetIteratorWithTotal rsIt = null;
    String string1 = "select * from ";
    String string2 = "select count(*) from ";
    String include;
    ResultSet rs = null;
    java.util.ArrayList Elements;
    if (idl.size() > 0) {
    where.append(" inner join ont_id on a.id = ont_id.id where 1=1 ");
    boolean b = insertIds(idl, connect);
    if (!b) {
        throw new org.osid.OperationFailedException("Failed adding id list to temp table") ;
            }

    } else {
    where.append(" where 1=1 ");
    }

   



    ArrayList dateTerms = new ArrayList<String>();
    dateTerms.add("DateFrom");
    dateTerms.add("DateTo");
    dateTerms.add("DateSince");

    for (int it = 0; it < termCount; it++) {
      qe = this.queryTerms.get(it);
      keySet.add(new KeyElement(qe.getType(), qe.getMatch(), qe.getDataType()));
      logger.debug(
          "Added Term element: "
              + qe.getType()
              + " "
              + Boolean.toString(qe.getMatch())
              + " "
              + qe.getDataType()
              + " "
              + keySet.size());
    }
    for (KeyElement ke : keySet) {
      if (!dateTerms.contains(ke.dataType)) {
        if (ke.match == true) {
          include = "  in  ";
        } else {
          include = "  not in  ";
        }
        where.append(" and (1, " + ke.type + ") " + include + "( ");
      } else {
        where.append(" and (");
      }
      logger.debug("Key Element: " + ke.type);
      java.util.List<QueryElement> filteredQuery =
          queryTerms
              .stream()
              .filter(q -> q.getType().equals(ke.type))
              .filter(q -> q.getMatch() == ke.match)
              .filter(q -> q.getDataType().equals(ke.dataType))
              .collect(Collectors.toList());
      Elements = new java.util.ArrayList<>();
      for (QueryElement q : filteredQuery) {
        logger.debug("Query Term: " + q.getId() + " " + q.getMatch());
        logger.debug("DataType :" + q.getDataType());
        switch (q.getDataType()) {
          case "DateFrom":
            Elements.add(" EventDate > to_date(?,'YYYY-MM-DD') ");
            break;
          case "DateSince":
            Elements.add(" EventDate > to_date(?,'YYYY-MM-DD') ");
            break;
          case "DateTo":
            Elements.add(" EventDate <= to_date(?,'YYYY-MM-DD') ");
            break;
          default:
            Elements.add("(1,?)");
        }
        parmArray.add(q.getId());
        parmTypeArray.add(q.getDataType());
      }
      logger.debug("String Check: " + String.join(" and ", Elements));
      where.append(String.join(",", Elements));
      where.append(")");
    }

    try {
      String selectStatementString = string1 + bq.toString() + where.toString();
      String countStatementString = string2 + bq.toString() + where.toString();
      logger.info("Select Statement = " + selectStatementString);
      logger.debug("Count Statement = " + countStatementString);

      PreparedStatement selectStatement = connect.prepareStatement(selectStatementString);
      PreparedStatement countStatement = connect.prepareStatement(countStatementString);
      for (int pi = 0; pi < parmArray.size(); pi++) {
        switch (parmTypeArray.get(pi)) {
          case "Int":
            selectStatement.setInt(pi + 1, Integer.valueOf(parmArray.get(pi)));
            countStatement.setInt(pi + 1, Integer.valueOf(parmArray.get(pi)));
            logger.debug(
                "Parm Type: "
                    + parmTypeArray.get(pi)
                    + " Parm: "
                    + (pi + 1)
                    + ": "
                    + parmArray.get(pi));
            break;
          case "String":
            selectStatement.setString(pi + 1, String.valueOf(parmArray.get(pi)));
            countStatement.setString(pi + 1, String.valueOf(parmArray.get(pi)));
            logger.debug(
                "Parm Type: "
                    + parmTypeArray.get(pi)
                    + " Parm: "
                    + (pi + 1)
                    + ": "
                    + parmArray.get(pi));
            break;
          case "DateFrom":
            selectStatement.setString(pi + 1, String.valueOf(parmArray.get(pi)));
            countStatement.setString(pi + 1, String.valueOf(parmArray.get(pi)));
            logger.debug(
                "Parm Type: "
                    + parmTypeArray.get(pi)
                    + " Parm: "
                    + (pi + 1)
                    + ": "
                    + parmArray.get(pi));
            break;
          case "DateTo":
            selectStatement.setString(pi + 1, String.valueOf(parmArray.get(pi)));
            countStatement.setString(pi + 1, String.valueOf(parmArray.get(pi)));
            logger.debug(
                "Parm Type: "
                    + parmTypeArray.get(pi)
                    + " Parm: "
                    + (pi + 1)
                    + ": "
                    + parmArray.get(pi));
            break;
          case "DateSince":
            selectStatement.setString(pi + 1, String.valueOf(parmArray.get(pi)));
            countStatement.setString(pi + 1, String.valueOf(parmArray.get(pi)));
            logger.debug(
                "Parm Type: "
                    + parmTypeArray.get(pi)
                    + " Parm: "
                    + (pi + 1)
                    + ": "
                    + parmArray.get(pi));
            break;
          default:
            throw new org.osid.OperationFailedException(
                "Valid parm data types are Int, String,DateFrom, DateTo, DateSince. "
                    + parmTypeArray.get(pi)
                    + " is not valid.");
        }
      }

      System.out.println(countStatement.toString());
      rs = countStatement.executeQuery();
      if (!rs.next()) {
        throw new SQLException("No records found.");
      }
      final int count = rs.getInt(1);
      rs.close();

      rs = selectStatement.executeQuery();
      rsIt = new ResultSetIteratorWithTotal<>(rs, selectStatement, connect);
      rsIt.setTotal(count);

    } catch (SQLException e) {
      throw new org.osid.OperationFailedException(e);
    } catch (Exception x) {
      x.printStackTrace();
    }

    return rsIt;
  }

    public Boolean insertIds(HashSet<String> programEntry, Connection connect) {
   System.out.println("Id loading...");	

  try {
    String query = "INSERT INTO ont_id (id) VALUES (?)";
    PreparedStatement preparedStatement = connect.prepareStatement(query);
    Iterator<String> pge = programEntry.iterator();
    while (pge.hasNext()) {
      String e = pge.next();
      System.out.println("Adding id: " + e);
      preparedStatement.setString(1,e);
      preparedStatement.addBatch();
    }
    preparedStatement.executeBatch();
    return true;
  } catch (SQLException e1) {
          e1.printStackTrace();
  }
    return false;

  }

}
