package org.dxtera.sis.impls.banner.cip.ontology.journaling.relevancy;

import java.util.List;
import org.dxtera.sis.impls.banner.cip.query.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// Cole Shaw, Amon Horne
// DXtera Institute
// September 2019
//
// Copyright (c) 2019 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicense, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// We want to extend from jamocha when we can, since it provides
// a lot of built-in functionality.
// We always want to state which OSID interfaces we are implementing.
public class BannerBranchQuery
    extends net.okapia.osid.jamocha.journaling.branch.spi.AbstractBranchQuery
    implements org.osid.journaling.BranchQuery, QueryTermInterface {

  java.util.ArrayList<QueryElement> queryTerms;
  Logger logger;

  public BannerBranchQuery() {
    queryTerms = new java.util.ArrayList<QueryElement>();
    logger = LoggerFactory.getLogger(BannerBranchQuery.class.getName());
  }

  @Override
  public void matchOriginJournalEntryId(org.osid.id.Id journalEntryId, boolean match) {
    this.queryTerms.add(
        new QueryElement("origin_journal_entry", journalEntryId.getIdentifier(), match, "Int"));
  }

  @Override
  public void clearOriginJournalEntryIdTerms() {
    this.clearTerms("origin_journal_entry");
  }

  @Override
  public void matchLatestJournalEntryId(org.osid.id.Id journalEntryId, boolean match) {
    this.queryTerms.add(
        new QueryElement("latest_journal_entry", journalEntryId.getIdentifier(), match, "Int"));
  }

  @Override
  public void clearLatestJournalEntryIdTerms() {
    this.clearTerms("latest_journal_entry");
  }

  @Override
  public void clearGenusTypeTerms() {
    this.clearTerms("pt_id");
  }

  // TODO: GENUS_TYPE is no longer PT_ID
  @Override
  public void matchGenusType(org.osid.type.Type type, boolean match) {
    this.queryTerms.add(new QueryElement("pt_id", type.getIdentifier(), match, "String"));
  }

  @Override
  public void matchId(org.osid.id.Id id, boolean match) {
    this.queryTerms.add(new QueryElement("id", id.getIdentifier(), match, "String"));
  }

  private void clearTerms(String type) {
    for (int i = 0; i < this.queryTerms.size(); i++) {
      if (queryTerms.get(i).getType().equals(type)) {
        this.queryTerms.remove(i);
      }
    }
  }

  public List<QueryElement> getQueryTerms() {
    List<QueryElement> listQE = (List) this.queryTerms;
    return listQE;
  }
}
