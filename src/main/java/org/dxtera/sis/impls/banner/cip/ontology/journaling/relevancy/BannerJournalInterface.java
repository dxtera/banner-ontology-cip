package org.dxtera.sis.impls.banner.cip.ontology.journaling.relevancy;

// This class is the basic Banner Journal, and
//   should be treated as the reference implementation for
//   a default, out-of-the-box Banner installation.
// Other, school-specific impls should extend this one
//   and may include customization information.
// For example, if a school stores entity data in a different
//   table, you can store that information in the school's
//   Journal (and thus it will be available to the
//   sessions).

// Scott Thorne (Templated by Jeff Merriman, Cole Shaw)
// DXtera
// February 2017
//
// Copyright (c) 2017 DXtera Institute. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicense, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

import org.dxtera.sis.impls.banner.cip.ontology.journaling.BranchGeneratorInterface;

// Typically we want to extend from net.okapia.osid.jamocha where possible,
// since that package contains a lot of helpful Abstract base
// classes and implementations.
public interface BannerJournalInterface extends org.osid.journaling.Journal {

  /**
   * This non-OSID method returns the JournalEntry generator. It is made available so that sessions
   * can share the same datbase configuration information for a given PeopleSoft implementation.
   * This needs to be public so it can be called in the subpackages.
   */
  public JournalEntryGeneratorInterface getJournalEntryGenerator();

  /**
   * This non-OSID method returns the Branch generator. It is made available so that sessions can
   * share the same datbase configuration information for a given PeopleSoft implementation. This
   * needs to be public so it can be called in the subpackages.
   */
  public BranchGeneratorInterface getBranchGenerator();
}
