/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dxtera.sis.impls.banner.csv;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import org.dxtera.sis.impls.banner.csv.beans.CipBean;
import org.dxtera.sis.impls.banner.csv.beans.CipBeanEnum;

public class CsvService {

  public static final String CIP_XLSX = "/cip/data/CIPCode2010.csv";

  public List<CipBean> getCIPCodes() {
    final List<CipBean> cipBeans = new ArrayList<>();
    List<String[]> readAll = CsvFileReader.readAll(CIP_XLSX, 1);

    Arrays.asList(readAll)
        .forEach(
            s -> {
              s.forEach(
                  (t) -> {
                    if (t.length == CipBeanEnum.values().length) {
                      List<String> chips = Arrays.asList(t);
                      CipBean chip = new CipBean();
                      chip.setCipFamily(chips.get(CipBeanEnum.CIPFAMILY.getIndex()).trim());
                      chip.setCipCode(chips.get(CipBeanEnum.CIPCODE.getIndex()).trim());
                      chip.setAction(chips.get(CipBeanEnum.ACTION.getIndex()).trim());
                      chip.setTextChange(
                          chips
                              .get(CipBeanEnum.TEXTCHANGE.getIndex())
                              .trim()
                              .equalsIgnoreCase("yes"));
                      chip.setCipTitle(chips.get(CipBeanEnum.CIPTITLE.getIndex()).trim());
                      chip.setCipDefinition(chips.get(CipBeanEnum.CIPDEFINITION.getIndex()).trim());
                      chip.setCipCrossreferences(
                          fixBlank(chips.get(CipBeanEnum.CIPCROSSREFERENCES.getIndex()).trim()));
                      chip.setExamples(fixBlank(chips.get(CipBeanEnum.EXAMPLES.getIndex()).trim()));
                      cipBeans.add(chip);
                    }
                  });
            });
    return cipBeans;
  }

  private String fixBlank(final String str) {
    if (Objects.nonNull(str) && !str.trim().isEmpty()) {
      return str.trim();
    }
    return null;
  }
}
