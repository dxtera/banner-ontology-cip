package org.dxtera.sis.impls.banner.cip.ontology;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import net.okapia.osid.jamocha.builder.hierarchy.hierarchy.MutableHierarchy;
import net.okapia.osid.jamocha.builder.ontology.subject.MutableSubject;
import net.okapia.osid.jamocha.id.id.ArrayIdList;
import net.okapia.osid.jamocha.nil.ontology.subject.EmptySubjectList;
import net.okapia.osid.jamocha.ontology.subject.ArraySubjectList;
import net.okapia.osid.primordium.id.BasicId;
import net.okapia.osid.primordium.locale.text.eng.us.Plain;
import org.dxtera.sis.impls.banner.cip.ontology.type.IdNamespaceConstants;
import org.dxtera.sis.impls.banner.cip.ontology.type.TypeConstants;
import org.osid.NotFoundException;
import org.osid.OperationFailedException;
import org.osid.PermissionDeniedException;
import org.osid.hierarchy.Node;
import org.osid.id.Id;
import org.osid.id.IdList;
import org.osid.ontology.Subject;
import org.osid.ontology.SubjectList;
import org.osid.ontology.SubjectNode;

// Scott Thorne (Templated by Jeff Merriman, Cole Shaw, Amon Horne)
// DXtera
// February 2017
//
// Copyright (c) 2017 DXtera Institute. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicense, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

// Typically we want to extend from net.okapia.osid.jamocha where possible,
// since that package contains a lot of helpful Abstract base
// classes and implementations.
// We always want to state which OSID interfaces we are implementing.
public class BannerSubjectHierarchySession
    extends net.okapia.osid.jamocha.ontology.spi.AbstractSubjectHierarchySession
    implements org.osid.ontology.SubjectHierarchySession {

  private static final String HIERARCHY_ROOT_IDENTIFIER = "cip-hierarchy-root";
  protected BannerOntologyInterface catalog;

  // This is the SubjectQuerySession that this SubjectLookupSession
  //    will be built upon.
  protected org.osid.ontology.SubjectLookupSession lookupSession;
  protected BannerOntologyManager mgr;

  // We use our BannerOntologyInterface for the catalog
  //   type here, instead of the generic OSID Ontology
  //   interface, since we know these implementations are tied
  //   together.
  public BannerSubjectHierarchySession(BannerOntologyManager mgr) throws OperationFailedException {

    super();
    this.mgr = mgr;
    this.catalog = mgr.catalog;
    this.lookupSession = mgr.getSubjectLookupSession();
    return;
  }

  @Override
  public boolean canAccessSubjectHierarchy() {
    return true;
  }

  @Override
  public org.osid.id.Id getSubjectHierarchyId() {
    try {

      return this.getSubjectHierarchy().getId();
    } catch (OperationFailedException | PermissionDeniedException ex) {
      throw new IllegalArgumentException(ex);
    }
  }

  @Override
  public org.osid.hierarchy.Hierarchy getSubjectHierarchy()
      throws org.osid.OperationFailedException, org.osid.PermissionDeniedException {
    MutableHierarchy mutableHierarchy = new MutableHierarchy();
    mutableHierarchy.setId(
        new BasicId(
            this.catalog.getId().getAuthority(),
            IdNamespaceConstants.HIERARCHY,
            this.catalog.getId().getIdentifier()));
    mutableHierarchy.setDisplayName(
        new Plain("Main Hierarchy for " + this.catalog.getDisplayName().getText()));
    mutableHierarchy.setDescription(
        new Plain("Main Hierarchy for " + this.catalog.getDisplayName().getText()));
    mutableHierarchy.setGenusType(TypeConstants.HIERARCHY);
    return mutableHierarchy;
  }

  @Override
  public IdList getRootSubjectIds() throws OperationFailedException, PermissionDeniedException {
    List<Id> rootSubjectIds = new ArrayList<>();
    SubjectList rootSubjects = this.getRootSubjects();
    while (rootSubjects.hasNext()) {
      Subject subject = rootSubjects.getNextSubject();
      rootSubjectIds.add(subject.getId());
    }
    return new ArrayIdList(rootSubjectIds);
  }

  @Override
  public SubjectList getRootSubjects() throws OperationFailedException, PermissionDeniedException {
    List<Subject> rootSubjects = new ArrayList<>();
    MutableSubject mutableSubject = new MutableSubject();
    mutableSubject.setId(
        BasicId.valueOf(
            IdNamespaceConstants.SUBJECT
                + ":"
                + HIERARCHY_ROOT_IDENTIFIER
                + "@"
                + this.catalog.getId().getAuthority()));
    mutableSubject.setDescription(new Plain("CIP Hierarchy Root"));
    mutableSubject.setDisplayName(new Plain("CIP Hierarchy Root"));
    mutableSubject.setGenusType(TypeConstants.CIP2010_SUBJECT);
    rootSubjects.add(mutableSubject);
    //    SubjectList subjects = lookupSession.getSubjects();
    //    while (subjects.hasNext()) {
    //      Subject subject = subjects.getNextSubject();
    //      if (checkIfRoot(subject.getId())) {
    //        rootSubjects.add(subject);
    //      }
    //    }
    return new ArraySubjectList(rootSubjects);
  }

  @Override
  public boolean hasParentSubjects(Id id)
      throws NotFoundException, OperationFailedException, PermissionDeniedException {
    validateId(id);
    if (isRoot().test(id)) {
      return false;
    }
    return true;
  }

  @Override
  public boolean isParentOfSubject(Id id, Id subjectId)
      throws NotFoundException, OperationFailedException, PermissionDeniedException {
    validateId(id);
    validateId(subjectId);
    if (checkIfParentAndChild(id, subjectId)) {
      return true;
    }
    return false;
  }

  @Override
  public IdList getParentSubjectIds(Id id)
      throws NotFoundException, OperationFailedException, PermissionDeniedException {
    validateId(id);
    SubjectList subjectList = this.getParentSubjects(id);
    List<Id> list = new ArrayList<>();
    while (subjectList.hasNext()) {
      Subject subject = subjectList.getNextSubject();
      list.add(subject.getId());
    }
    return new ArrayIdList(list);
  }

  @Override
  public SubjectList getParentSubjects(Id id)
      throws NotFoundException, OperationFailedException, PermissionDeniedException {
    validateId(id);
    // root has no parents
    if (isRoot().test(id)) {
      return new EmptySubjectList();
    }
    // if this is level 1 id, then root is the parent
    if (!cipContainsDot().test(id)) {
      return this.getRootSubjects();
    }
    SubjectList subjectList = this.lookupSession.getSubjects();
    List<Subject> list = new ArrayList<>();
    while (subjectList.hasNext()) {
      Subject subject = subjectList.getNextSubject();
      if (this.checkIfParentAndChild(subject.getId(), id)) {
        list.add(subject);
      }
    }
    return new ArraySubjectList(list);
  }

  @Override
  public boolean isAncestorOfSubject(Id id, Id subjectId)
      throws NotFoundException, OperationFailedException, PermissionDeniedException {
    validateId(id);
    validateId(subjectId);
    if (this.isParentOfSubject(id, subjectId)) {
      return true;
    }
    IdList parentIds = this.getParentSubjectIds(subjectId);
    while (parentIds.hasNext()) {
      Id parentId = parentIds.getNextId();
      if (isAncestorOfSubject(id, parentId)) {
        return true;
      }
    }
    return false;
  }

  @Override
  public boolean hasChildSubjects(Id id)
      throws NotFoundException, OperationFailedException, PermissionDeniedException {
    validateId(id);
    SubjectList children = this.getChildSubjects(id);
    if (children.hasNext()) {
      return true;
    }
    return false;
  }

  @Override
  public boolean isChildOfSubject(Id id, Id subjectId)
      throws NotFoundException, OperationFailedException, PermissionDeniedException {
    // reverse the check to see if is child
    if (this.isParentOfSubject(subjectId, id)) {
      return true;
    }
    return false;
  }

  @Override
  public IdList getChildSubjectIds(Id id)
      throws NotFoundException, OperationFailedException, PermissionDeniedException {
    validateId(id);
    SubjectList subjectList = this.getChildSubjects(id);
    List<Id> list = new ArrayList<>();
    while (subjectList.hasNext()) {
      Subject subject = subjectList.getNextSubject();
      list.add(subject.getId());
    }
    return new ArrayIdList(list);
  }

  @Override
  public SubjectList getChildSubjects(Id id)
      throws NotFoundException, OperationFailedException, PermissionDeniedException {
    validateId(id);
    SubjectList subjectList = this.lookupSession.getSubjects();
    List<Subject> list = new ArrayList<>();
    while (subjectList.hasNext()) {
      Subject subject = subjectList.getNextSubject();
      // reverse the ids to check if is child
      if (this.checkIfParentAndChild(id, subject.getId())) {
        list.add(subject);
      }
    }
    return new ArraySubjectList(list);
  }

  @Override
  public boolean isDescendantOfSubject(Id id, Id subjectId)
      throws NotFoundException, OperationFailedException, PermissionDeniedException {
    validateId(id);
    validateId(subjectId);
    // reverse the sense
    if (this.checkIfParentAndChild(subjectId, id)) {
      return true;
    }
    IdList childIds = this.getChildSubjectIds(subjectId);
    while (childIds.hasNext()) {
      Id childId = childIds.getNextId();
      if (isDescendantOfSubject(id, childId)) {
        return true;
      }
    }
    return false;
  }

  @Override
  public Node getSubjectNodeIds(
      Id subjectId, long ancestorLevels, long descendantLevels, boolean includeSiblings)
      throws NotFoundException, OperationFailedException, PermissionDeniedException {
    return this.buildSubjectNodes(subjectId, ancestorLevels, descendantLevels, includeSiblings);
  }

  @Override
  public SubjectNode getSubjectNodes(
      Id subjectId, long ancestorLevels, long descendantLevels, boolean includeSiblings)
      throws NotFoundException, OperationFailedException, PermissionDeniedException {
    return this.buildSubjectNodes(subjectId, ancestorLevels, descendantLevels, includeSiblings);
  }

  private BannerSubjectNode buildSubjectNodes(
      Id subjectId, long ancestorLevels, long descendantLevels, boolean includeSiblings)
      throws NotFoundException, OperationFailedException, PermissionDeniedException {
    validateId(subjectId);
    Subject subject = this.lookupSession.getSubject(subjectId);
    BannerSubjectNode node = new BannerSubjectNode();
    node.setId(subjectId);
    node.setSubject(subject);
    addParents(node, ancestorLevels);
    addChildren(node, descendantLevels);
    if (includeSiblings) {
      for (BannerSubjectNode parent : node.getParentNodes().values()) {
        addChildren(parent, 1);
      }
    }
    return node;
  }

  private void addParents(BannerSubjectNode node, long ancestorLevels)
      throws NotFoundException, OperationFailedException, PermissionDeniedException {
    //        System.out.println("addParents " + node.getId().getIdentifier() + " " +
    // ancestorLevels);
    SubjectList subjectList = this.getParentSubjects(node.getId());
    if (subjectList.hasNext()) {
      //  spec: ancestorLevels A value of 0 returns no parents in the node.
      // if only going up so many levels just record that there are more levels
      // but don't actually populate them
      node.setHasParents(true);
    }
    if (ancestorLevels <= 0) {
      return;
    }
    ancestorLevels--;
    while (subjectList.hasNext()) {
      Subject parentSubject = subjectList.getNextSubject();
      BannerSubjectNode parentNode = new BannerSubjectNode();
      parentNode.setId(parentSubject.getId());
      parentNode.setSubject(parentSubject);
      node.getParentNodes().put(parentNode.getId(), parentNode);
      parentNode.getChildNodes().put(node.getId(), node);
      // recurse up the tree
      this.addParents(parentNode, ancestorLevels);
    }
  }

  private void addChildren(BannerSubjectNode node, long descendantLevels)
      throws NotFoundException, OperationFailedException, PermissionDeniedException {
    //        System.out.println("addChildren " + node.getId().getIdentifier() + " " +
    // descendantLevels);
    SubjectList subjectList = this.getChildSubjects(node.getId());
    if (subjectList.hasNext()) {
      // spec: descendantLevels A value of 0 returns no children in the node.
      // if only going down so many levels just record that there are more levels
      // but don't actually populate them
      node.setHasChildren(true);
    }
    if (descendantLevels <= 0) {
      return;
    }
    descendantLevels--;
    while (subjectList.hasNext()) {
      Subject childSubject = subjectList.getNextSubject();
      BannerSubjectNode childNode = new BannerSubjectNode();
      childNode.setId(childSubject.getId());
      childNode.setSubject(childSubject);
      node.getChildNodes().put(childNode.getId(), childNode);
      childNode.getParentNodes().put(node.getId(), node);
      // recurse up the tree
      this.addChildren(childNode, descendantLevels);
    }
  }

  // protected so it can be tested
  //  protected boolean checkIfRoot(Id id) {
  //
  //    // CIPs with 2 characters are roots
  //    if (id.getIdentifier().startsWith(IdNamespaceConstants.CIP_ID_PREFIX)) {
  //      int codeLength = id.getIdentifier().length() -
  // IdNamespaceConstants.CIP_ID_PREFIX.length();
  //      if (codeLength == 2) {
  //        return true;
  //      }
  //      return false;
  //    }
  //    // all SOC's are all roots
  //    return true;
  //  }

  private void validateId(Id id) throws NotFoundException {
    if (!id.getAuthority().equals(this.catalog.getId().getAuthority())) {
      throw new NotFoundException(
          "Id has wrong authority expected "
              + this.catalog.getId().getAuthority()
              + " found "
              + id.getAuthority());
    }
    if (!id.getIdentifierNamespace().equals(IdNamespaceConstants.SUBJECT)) {
      throw new NotFoundException(
          "Id has wrong namesspace expected "
              + IdNamespaceConstants.SUBJECT
              + " found "
              + id.getIdentifierNamespace());
    }
    if (id.getIdentifier() == null) {
      throw new NotFoundException("Id has null identifier");
    }
  }

  private static Predicate<Id> isRoot() {
    return id -> id.getIdentifier().equals(HIERARCHY_ROOT_IDENTIFIER);
  }

  private static Predicate<Id> cipContainsDot() {
    return id ->
        id.getIdentifier().replaceAll(IdNamespaceConstants.CIP_ID_PREFIX, "").contains(".");
  }

  // protected so it can be tested
  protected boolean checkIfParentAndChild(Id parentId, Id childId) {
    // check if parent id is root and if child doesn't have ".", then return true

    if (isRoot().test(parentId)) {
      if (!cipContainsDot().test(childId)) {
        return true;
      }
      return false;
    } else if (isRoot().test(childId)) {
      // root cannot be anyone's child
      return false;
    }

    if (parentId.getIdentifier().startsWith(IdNamespaceConstants.CIP_ID_PREFIX)) {
      String parentCode =
          parentId.getIdentifier().substring(IdNamespaceConstants.CIP_ID_PREFIX.length());
      if (childId.getIdentifier().startsWith(IdNamespaceConstants.CIP_ID_PREFIX)) {
        String childCode =
            childId.getIdentifier().substring(IdNamespaceConstants.CIP_ID_PREFIX.length());
        // not a child of itself
        if (childCode.equals(parentCode)) {
          return false;
        }
        if (!childCode.startsWith(parentCode)) {
          return false;
        }
        int diffInLength = childCode.length() - parentCode.length();
        if (diffInLength > 3) {
          return false;
        }
        return true;
      }
    }
    // SOC's are all roots no parents and no children
    return false;
  }
}
