package org.dxtera.sis.impls.banner.cip.ontology.type;

import static net.okapia.osid.primordium.locale.text.eng.us.Plain.text;

import java.util.Objects;
import org.osid.locale.DisplayText;
import org.osid.type.Type;

/**
 * Bean version of Type
 *
 * <p>Note: a type is really an ID with more data describing it...
 *
 * @author nwright
 */
public class TypeBean implements org.osid.type.Type {

  private String authority;
  private String identifier;
  private String identifierNamespace;
  private DisplayText displayLabel;
  private DisplayText displayName;
  private DisplayText description;
  private DisplayText domain;

  public TypeBean() {}

  public TypeBean(String authority, String identifierNamespace, String identifier) {
    this.authority = authority;
    this.identifierNamespace = identifierNamespace;
    this.identifier = identifier;
  }

  public static Type copy(Type source) {
    if (source == null) {
      return null;
    }
    TypeBean bean = new TypeBean();
    bean.setAuthority(source.getAuthority());
    bean.setIdentifierNamespace(source.getIdentifierNamespace());
    bean.setIdentifier(source.getIdentifier());
    bean.setDescription(text(source.getDescription().toString()));
    bean.setDisplayLabel(text(source.getDisplayLabel().toString()));
    bean.setDisplayName(text(source.getDisplayName().toString()));
    bean.setDomain(text(source.getDomain().toString()));
    return bean;
  }

  @Override
  public String getAuthority() {
    return authority;
  }

  public void setAuthority(String authority) {
    this.authority = authority;
  }

  @Override
  public String getIdentifier() {
    return identifier;
  }

  public void setIdentifier(String identifier) {
    this.identifier = identifier;
  }

  @Override
  public String getIdentifierNamespace() {
    return identifierNamespace;
  }

  public void setIdentifierNamespace(String identifierNamespace) {
    this.identifierNamespace = identifierNamespace;
  }

  ////
  //// other non-id information
  ////
  @Override
  public DisplayText getDisplayLabel() {
    return displayLabel;
  }

  public void setDisplayLabel(DisplayText displayLabel) {
    this.displayLabel = displayLabel;
  }

  @Override
  public DisplayText getDisplayName() {
    return displayName;
  }

  public void setDisplayName(DisplayText displayName) {
    this.displayName = displayName;
  }

  @Override
  public DisplayText getDescription() {
    return description;
  }

  public void setDescription(DisplayText description) {
    this.description = description;
  }

  @Override
  public DisplayText getDomain() {
    return domain;
  }

  public void setDomain(DisplayText domain) {
    this.domain = domain;
  }

  ////
  //// implement type
  ////
  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (!(obj instanceof Type)) {
      return false;
    }
    final Type other = (Type) obj;
    if (!Objects.equals(this.authority, other.getAuthority())) {
      return false;
    }
    if (!Objects.equals(this.identifier, other.getIdentifier())) {
      return false;
    }
    if (!Objects.equals(this.identifierNamespace, other.getIdentifierNamespace())) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    int hash = 7;
    hash = 79 * hash + Objects.hashCode(this.authority);
    hash = 79 * hash + Objects.hashCode(this.identifier);
    hash = 79 * hash + Objects.hashCode(this.identifierNamespace);
    return hash;
  }

  @Override
  public int compareTo(Type type) {
    if (this.equals(type)) {
      return 0;
    }
    // nulls sort first
    if (type == null) {
      return 1;
    }
    return toComparableString(this).compareTo(this.toComparableString(type));
  }

  private String toComparableString(Type type) {
    return this.toComparableString(
        type.getAuthority(), type.getIdentifierNamespace(), type.getIdentifier());
  }

  private String toComparableString(
      String authority, String identifierNamespace, String identifier) {
    StringBuilder sb = new StringBuilder();
    this.appendIfNotNull(sb, authority);
    sb.append("|"); // delimiter
    this.appendIfNotNull(sb, identifierNamespace);
    sb.append("|"); // delimiter
    this.appendIfNotNull(sb, identifier);
    sb.append("|"); // delimiter
    return sb.toString();
  }

  private void appendIfNotNull(StringBuilder sb, String text) {
    if (text == null) {
      return;
    }
    sb.append(text);
  }
}
