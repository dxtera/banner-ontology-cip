package org.dxtera.sis.impls.banner.cip.query;

class KeyElement {
  String type;
  Boolean match;
  String dataType;

  KeyElement(String type, Boolean match, String dataType) {
    this.type = type;
    this.match = match;
    this.dataType = dataType;
  }

  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    KeyElement ke = (KeyElement) obj;
    if (type.equals(ke.type) && this.match == ke.match) {
      return true;
    } else {
      return false;
    }
  }

  public int hashCode() {
    String test = this.type + "|" + Boolean.toString(match);
    return test.hashCode();
  }
}
