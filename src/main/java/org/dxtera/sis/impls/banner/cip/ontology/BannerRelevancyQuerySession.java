package org.dxtera.sis.impls.banner.cip.ontology;

import com.zaxxer.hikari.HikariDataSource;
import java.sql.Connection;
import org.dxtera.sis.impls.banner.cip.query.QueryAssembler;
import org.dxtera.utils.iterators.ontology.RelevancyListIteratorImpl;
import org.osid.OperationFailedException;

// Cole Shaw, Amon Horne
// DXtera Institute
// September 2019
//
// Copyright (c) 2019 DXtera Institute. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicense, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

// Typically we want to extend from net.okapia.osid.jamocha where possible,
// since that package contains a lot of helpful Abstract base
// classes and implementations.
// We always want to state which OSID interfaces we are implementing.
public class BannerRelevancyQuerySession
    extends net.okapia.osid.jamocha.ontology.spi.AbstractRelevancyQuerySession
    implements org.osid.ontology.RelevancyQuerySession {

  protected BannerOntologyInterface catalog;
  protected RelevancyGeneratorInterface generator;
  protected BannerOntologyManager mgr;

  protected Integer bufferSize = 100;
  protected HikariDataSource ds = null;
  protected String sqlpath;

  // We use our BannerOntologyInterface for the catalog
  //   type here, instead of the generic OSID Ontology
  //   interface, since we know these implementations are tied
  //   together.
  public BannerRelevancyQuerySession(BannerOntologyManager mgr)
      throws org.osid.OperationFailedException {

    super();
    this.mgr = mgr;
    setOntology(mgr.catalog);
    this.catalog = mgr.catalog;
    this.generator = this.catalog.getRelevancyGenerator();
    AuthorityInterface aitest = (AuthorityInterface) this.generator;
    aitest.setAuthority(mgr.authority);

    return;
  }

  @Override
  public org.osid.ontology.RelevancyQuery getRelevancyQuery() {
    // Need a new assembler for every RelevancyQuery -- don't want the assembler
    //   values persisting with the session...
    // But, probably should have a way to get the Query's assembler, not
    //   the session one -- to make sure that you grab the right data.
    // Probably a factor if multiple simultaneous consumers with the same session,
    //   not necessary for vanilla use case?
    return new BannerRelevancyQuery();
  }

  @Override
  public org.osid.ontology.RelevancyList getRelevanciesByQuery(
      org.osid.ontology.RelevancyQuery query)
      throws OperationFailedException, org.osid.PermissionDeniedException {

    try {
      Connection connect = mgr.getConnection();
      org.dxtera.utils.iterators.core.ResultSetIteratorWithTotal<org.osid.ontology.Relevancy> rsIt =
          null;
      QueryAssembler qa = new QueryAssembler("RelevancyQuery", mgr.authority);
      rsIt = qa.processQuery(connect, (org.osid.OsidObjectQuery) query, "ontology");

      AuthorityInterface aitest = (AuthorityInterface) this.generator;
      aitest.setAuthority(this.mgr.authority);

      rsIt.setConverter(this.generator);

      return new RelevancyListIteratorImpl(rsIt, rsIt.getTotal());
    } catch (Exception x) {
      x.printStackTrace();
      throw new org.osid.OperationFailedException(x);
    }
  }
}
