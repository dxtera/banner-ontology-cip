package org.dxtera.sis.impls.banner.cip.ontology.journaling.subject;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.InputStream;
import java.util.HashMap;
import net.okapia.osid.jamocha.builder.journaling.branch.MutableBranch;
import net.okapia.osid.jamocha.builder.journaling.journalentry.MutableJournalEntry;
import net.okapia.osid.primordium.id.BasicId;
import net.okapia.osid.primordium.locale.text.eng.us.Plain;
import org.dxtera.sis.impls.banner.cip.ontology.AuthorityInterface;
import org.dxtera.sis.impls.banner.cip.ontology.type.TypeConstants;
import org.dxtera.sis.impls.banner.csv.beans.CipBean;
import org.osid.OperationFailedException;
import org.osid.type.Type;

// This class is the ``JournalEntry`` entity. Until we
//   find out otherwise, we'll use a single JournalEntry
//   for all the journaling implementations.
// If we find out that there are JournalEntry variants,
//   then we may need to create implementation-specific
//   JournalEntry classes.

// Scott Thorne, Cole Shaw, Amon Horne
// Okapia
// February 2017
//
// Copyright (c) 2017 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicense, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// We want to extend from jamocha when we can, since it provides
// a lot of built-in functionality.
// We always want to state which OSID interfaces we are implementing.
// We also implement the Generator interface so that
// we can use the protected setters from
// jamocha.
public class BannerJournalEntry
    extends net.okapia.osid.jamocha.journaling.journalentry.spi.AbstractJournalEntry
    implements org.osid.journaling.JournalEntry,
        JournalEntryGeneratorInterface,
        AuthorityInterface {

  protected String authority;

  public String getAuthority() {

    return this.authority;
  }

  public void setAuthority(String authority) {
    this.authority = authority;
  }

  public BannerJournalEntry(String authority) throws OperationFailedException {
    super();
    this.authority = authority;
  }

  public BannerJournalEntry() throws OperationFailedException {
    super();
    this.authority = "dxtera";
  }

  public HashMap<String, String> getMapFile(String fileName) {
    StringBuilder filePath = new StringBuilder();
    String basePath = "cip/map/journaling/" + fileName;
    String extPath = "cip/map/" + authority + "/journaling/" + fileName;
    HashMap<String, String> jsonMap = new HashMap<>();
    try {
      ObjectMapper objectMapper = new ObjectMapper();
      InputStream input =
          (Thread.currentThread().getContextClassLoader().getResourceAsStream(extPath.toString())
                  != null)
              ? Thread.currentThread()
                  .getContextClassLoader()
                  .getResourceAsStream(extPath.toString())
              : Thread.currentThread()
                  .getContextClassLoader()
                  .getResourceAsStream(basePath.toString());
      jsonMap = objectMapper.readValue(input, new TypeReference<HashMap<String, String>>() {});
    } catch (Exception e) {
      e.printStackTrace();
    }
    return jsonMap;
  }

  // This method takes an SQL ResultSet and converts
  //   it into a JournalEntry object.
  @Override
  public org.osid.journaling.JournalEntry convert(CipBean cipBean) throws OperationFailedException {

    String idPrefix = "naics.2017.";
    MutableJournalEntry mutableJournalEntry = new MutableJournalEntry();
    mutableJournalEntry.setId(
        new BasicId(this.authority, "JournalEntry", idPrefix + cipBean.getCipCode()));
    mutableJournalEntry.setDescription(new Plain(calcDescription(cipBean)));
    mutableJournalEntry.setDisplayName(new Plain(calcName(cipBean)));
    mutableJournalEntry.setGenusType(toGenusType(cipBean));
    mutableJournalEntry.setSourceId(
        new BasicId(this.authority, "Subject", mutableJournalEntry.getId().getIdentifier()));

    MutableBranch branch = new MutableBranch();
    branch.setId(new BasicId(this.authority, "Branch", "BannerSubjectBranch"));
    mutableJournalEntry.setBranch(branch);
    return mutableJournalEntry;
  }

  protected String calcName(CipBean deptEd) {
    StringBuilder sb = new StringBuilder();
    sb.append(deptEd.getCipCode());
    sb.append(" -- ");
    sb.append(deptEd.getCipTitle());
    return sb.toString();
  }

  protected String calcDescription(CipBean deptEd) {
    StringBuilder sb = new StringBuilder();
    sb.append(deptEd.getCipDefinition());
    sb.append("\n");
    sb.append("\n");
    sb.append("\nCrosssreferences: ");
    sb.append(deptEd.getCipCrossreferences());
    sb.append("\n");
    sb.append("\n");
    sb.append("\nExamples: ");
    sb.append(deptEd.getExamples());
    return sb.toString();
  }

  /**
   * Intended to be overridden by a institution
   *
   * @param subject
   * @return The genus type, Fall, Spring, etc...
   */
  protected Type toGenusType(CipBean subject) {
    return TypeConstants.CIP2010_SUBJECT;
  }

  /**
   * This method copies an "unknown" provider JournalEntry into a JournalEntry object from this
   * package. This is often helpful to ensure that a JournalEntry behaves as expected.
   */
  public org.osid.journaling.JournalEntry convert(org.osid.journaling.JournalEntry other)
      throws OperationFailedException {

    // Note that we use our package's JournalEntry object
    //   so that we can access the setter methods.
    BannerJournalEntry journalEntry = new BannerJournalEntry(this.authority);

    journalEntry.setId(other.getId());
    journalEntry.setDescription(other.getDescription());
    journalEntry.setDisplayName(other.getDisplayName());
    journalEntry.setGenusType(other.getGenusType());

    MutableBranch branch = new MutableBranch();
    branch.setId(other.getBranchId());
    journalEntry.setBranch(branch);
    journalEntry.setSourceId(BasicId.valueOf(other.getSourceId()));

    // Once we support record types, you should also check
    //   and call addRecord() here.

    // if (hasRecordType(other.getGenusType())) {
    //     addRecord(other.getTermRecord(other.getGenusType()));
    // }

    return journalEntry;
  }

  // If you need to, you can override the default Jamocha
  // behavior for this method.
  // @Override
  // public org.osid.id.Id getBranchId() {
  //   return super.getBranchId();
  // }

  // If you need to, you can override the default Jamocha
  // behavior for this method.
  // @Override
  // protected void setBranch(org.osid.id.Id id) {
  //   super.setBranch(id);
  // }

  // If you need to, you can override the default Jamocha
  // behavior for this method.
  // @Override
  // public org.osid.journaling.Branch getBranch()
  //    throws OperationFailedException {
  //
  //  return super.getBranch();
  // }

  // If you need to, you can override the default Jamocha
  // behavior for this method.
  // @Override
  // public org.osid.id.Id getSourceId() {
  //  return super.getSourceId();
  // }

  // If you need to, you can override the default Jamocha
  // behavior for this method.
  // @Override
  // protected void setSource(org.osid.id.Id id) {
  //   super.setSource(id);
  // }
}
