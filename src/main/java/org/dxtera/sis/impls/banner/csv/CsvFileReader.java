/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dxtera.sis.impls.banner.csv;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/** @author mansi */
public class CsvFileReader {

  private static List<String[]> readAll(InputStream input) throws Exception {
    List<String[]> list = new ArrayList<>();
    try (CSVReader csvReader = new CSVReader(new InputStreamReader(input))) {
      list.addAll(csvReader.readAll());
      input.close();
    }
    return list;
  }

  private static List<String[]> readAll(InputStream inputStream, int skip) throws Exception {
    List<String[]> list = new ArrayList<>();
    try (CSVReader csvReader =
        new CSVReaderBuilder(new InputStreamReader(inputStream)).withSkipLines(skip).build()) {
      //      csvReader.skip(skip);
      list.addAll(csvReader.readAll());
      inputStream.close();
    }
    return list;
  }

  private static List<String[]> readAll(
      InputStream inputStream, int skip, char separator, boolean ignoreQuotations)
      throws Exception {
    List<String[]> list = new ArrayList<>();
    final CSVParser parser =
        new CSVParserBuilder()
            .withSeparator(separator)
            .withIgnoreQuotations(ignoreQuotations)
            .build();
    try (CSVReader csvReader =
        new CSVReaderBuilder(new InputStreamReader(inputStream))
            .withSkipLines(skip)
            .withCSVParser(parser)
            .build()) {
      list.addAll(csvReader.readAll());
      inputStream.close();
    }
    return list;
  }

  public static List<String[]> readAll(String file) {
    List<String[]> list = new ArrayList<>();
    try {
      list.addAll(readAll(CsvService.class.getResourceAsStream(file)));
    } catch (Exception ex) {
      throw new RuntimeException(ex);
    }
    return list;
  }

  public static List<String[]> readAll(String file, int skip) {
    List<String[]> list = new ArrayList<>();
    try {
      list.addAll(readAll(CsvService.class.getResourceAsStream(file), skip));
    } catch (Exception ex) {
      throw new RuntimeException(ex);
    }
    return list;
  }

  public static List<String[]> readAll(
      String file, int skip, char separator, boolean ignoreQuotations) {
    List<String[]> list = new ArrayList<>();
    try {
      list.addAll(
          readAll(CsvService.class.getResourceAsStream(file), skip, separator, ignoreQuotations));
    } catch (Exception ex) {
      throw new RuntimeException(ex);
    }
    return list;
  }
}
