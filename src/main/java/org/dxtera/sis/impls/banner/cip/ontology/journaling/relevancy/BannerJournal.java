package org.dxtera.sis.impls.banner.cip.ontology.journaling.relevancy;

import static net.okapia.osid.primordium.locale.text.eng.us.Plain.text;

import net.okapia.osid.primordium.id.BasicId;
import org.dxtera.sis.impls.banner.cip.ontology.journaling.BranchGeneratorInterface;
import org.dxtera.sis.impls.banner.cip.ontology.type.TypeConstants;
import org.osid.OperationFailedException;

// This class is the basic Banner Journal, and
//   should be treated as the reference implementation for
//   a default, out-of-the-box Banner installation.
// Other, school-specific impls should extend this one
//   and may include customization information.
// For example, if a school stores course data in a different
//   table, you can store that information in the school's
//   Journal (and thus it will be available to the
//   sessions).

// Scott Thorne (Templated by Jeff Merriman, Cole Shaw, Amon Horne)
// DXtera
// February 2017
//
// Copyright (c) 2017 DXtera Institute. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicense, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

// Typically we want to extend from net.okapia.osid.jamocha where possible,
// since that package contains a lot of helpful Abstract base
// classes and implementations.
// We always want to state which OSID interfaces we are implementing.
public class BannerJournal extends net.okapia.osid.jamocha.journaling.journal.spi.AbstractJournal
    implements BannerJournalInterface, org.osid.journaling.Journal {

  protected String authority;
  // Note that we tend to provide fully qualified
  //   class paths inline, instead of importing
  //   at the top of the file and calling just
  //   "Type" here. That way we avoid any
  //   confusion with the package's Type class (if present)
  //   and the org.osid Type interface.
  private final org.osid.type.Type GENUS_TYPE;

  // Each Journal requires a set of entity Generators
  //   that encapsulates knowledge about the tables and columns.
  private JournalEntryGeneratorInterface journalEntryGenerator;
  private BranchGeneratorInterface branchGenerator;

  /**
   * Constructs a Journal based on an Id.
   *
   * @param journalId the Id of the Journal
   */
  public BannerJournal(org.osid.id.Id journalId) throws OperationFailedException {
    java.lang.String identifier = journalId.getIdentifier();

    current();
    this.authority = journalId.getAuthority();

    this.GENUS_TYPE = TypeConstants.RELEVANCY_JOURNAL;

    this.journalEntryGenerator = new BannerJournalEntry(authority);
    this.branchGenerator = new BannerBranch(authority);

    setId(journalId);
    setDisplayName(text("Journal " + identifier));
    setDescription(text("This is the " + identifier + " Journal."));
    setGenusType(GENUS_TYPE);

    return;
  }

  /**
   * Constructs a Journal based on a string identifier.
   *
   * @param identifier string identifier
   */
  public BannerJournal(String identifier) throws OperationFailedException {
    this(new BasicId("temp", "Journal", identifier));
    return;
  }

  /**
   * Constructs a Journal based on a numeric identifier.
   *
   * @param identifier the identifier
   */
  public BannerJournal(long identifier) throws OperationFailedException {
    this(String.valueOf(identifier));
    return;
  }

  /**
   * This non-OSID method returns the JournalEntry generator. It is made available so that sessions
   * can share the same datbase configuration information for a given PeopleSoft implementation.
   * This needs to be public so it can be called in the subpackages.
   */
  public JournalEntryGeneratorInterface getJournalEntryGenerator() {
    return this.journalEntryGenerator;
  }
  /**
   * This non-OSID method returns the Branch generator. It is made available so that sessions can
   * share the same datbase configuration information for a given PeopleSoft implementation. This
   * needs to be public so it can be called in the subpackages.
   */
  public BranchGeneratorInterface getBranchGenerator() {
    return this.branchGenerator;
  }
}
