package org.dxtera.sis.impls.banner.cip.ontology.journaling.subject;

import static net.okapia.osid.primordium.locale.text.eng.us.Plain.text;

import net.okapia.osid.jamocha.builder.journaling.branch.MutableBranch;
import net.okapia.osid.jamocha.journaling.branch.MutableBranchList;
import net.okapia.osid.primordium.id.BasicId;
import org.dxtera.sis.impls.banner.cip.ontology.journaling.BranchGeneratorInterface;
import org.dxtera.sis.impls.banner.cip.ontology.type.TypeConstants;
import org.osid.binding.java.annotation.OSID;

// by-hand
//
// Scott Thorne (Templated by Jeff Merriman, Cole Shaw, Amon Horne)
// DXtera
// February 2017
//
// Copyright (c) 2017 DXtera Institute. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicense, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

// Typically we want to extend from net.okapia.osid.jamocha where possible,
// since that package contains a lot of helpful Abstract base
// classes and implementations.
// We always want to state which OSID interfaces we are implementing.
public class BannerBranchLookupSession
    extends net.okapia.osid.jamocha.journaling.spi.AbstractBranchLookupSession
    implements org.osid.journaling.BranchLookupSession {

  private BannerJournalInterface catalog;
  private BranchGeneratorInterface generator;
  private String institution;
  private BannerJournalingManager mgr;

  // We use our BannerJournalInterface for the catalog
  //   type here, instead of the generic OSID Journal
  //   interface, since we know these implementations are tied
  //   together.
  protected BannerBranchLookupSession(BannerJournalingManager mgr)
      throws org.osid.OperationFailedException {

    super();
    this.mgr = mgr;
    setJournal(mgr.catalog);
    this.catalog = mgr.catalog;
    this.institution = mgr.authority;
    this.generator = this.catalog.getBranchGenerator();
    return;
  }

  protected BannerBranchLookupSession(org.osid.proxy.Proxy proxy, BannerJournalInterface catalog)
      throws org.osid.OperationFailedException {

    super();
    setJournal(catalog);
    this.catalog = catalog;
    return;
  }

  @Override
  public boolean canLookupBranches() {
    return true;
  }

  // by-hand
  @OSID
  @Override
  public org.osid.journaling.Branch getBranch(org.osid.id.Id branchId)
      throws org.osid.OperationFailedException, org.osid.PermissionDeniedException,
          org.osid.NotFoundException {

    MutableBranch branch = new MutableBranch();

    branch.setId(new BasicId(this.institution, "Branch", "BannerSubjectBranch"));
    // branch.setActive(true);
    branch.current();
    branch.setDisplayName(text("Main Branch for " + this.institution));
    branch.setDescription(text("Main Branch for " + this.institution));

    branch.setGenusType((org.osid.type.Type) TypeConstants.SUBJECT_BRANCH);
    branch.setEnabled(true);
    branch.setOperational(true);
    // don't know what to put here
    // branch.setOriginJournalEntry(null);
    // branch.setLatestJournalEntry(null);

    return branch;
  }

  // If we do not need to override the Jamocha implementation, then
  //    we can leave these methods out or just call super().
  // @Override
  // public org.osid.journaling.BranchList getBranchesByParentGenusType(org.osid.type.Type
  // branchGenusType)
  //     throws org.osid.OperationFailedException,
  //            org.osid.PermissionDeniedException {

  //   return super.getBranchesByParentGenusType(branchGenusType);
  // }

  // If we do not need to override the Jamocha implementation, then
  //    we can leave these methods out or just call super().
  // @Override
  // public org.osid.journaling.BranchList getBranchesByRecordType(org.osid.type.Type
  // branchRecordType)
  //     throws org.osid.OperationFailedException,
  //            org.osid.PermissionDeniedException {

  //   return super.getBranchesByRecordType(branchRecordType);
  // }

  @OSID
  @Override
  // The BranchList that is returned should be an iterator so that
  //   we don't try to unpack all the results into memory.
  public org.osid.journaling.BranchList getBranches()
      throws org.osid.OperationFailedException, org.osid.PermissionDeniedException {
    try {
      org.osid.journaling.Branch branch =
          this.getBranch(new BasicId(this.institution, "branch", "main"));
      org.osid.journaling.BranchList bl = new MutableBranchList(branch);
      return bl;
    } catch (Exception e) {
      throw new org.osid.OperationFailedException(e);
    }
  }
}
