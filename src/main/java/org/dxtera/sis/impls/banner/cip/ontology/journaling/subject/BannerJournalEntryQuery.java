package org.dxtera.sis.impls.banner.cip.ontology.journaling.subject;

import java.util.List;
import org.dxtera.sis.impls.banner.cip.query.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// Cole Shaw, Amon Horne
// DXtera Institute
// September 2019
//
// Copyright (c) 2019 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicense, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// We want to extend from jamocha when we can, since it provides
// a lot of built-in functionality.
// We always want to state which OSID interfaces we are implementing.
public class BannerJournalEntryQuery
    extends net.okapia.osid.jamocha.journaling.journalentry.spi.AbstractJournalEntryQuery
    implements org.osid.journaling.JournalEntryQuery, QueryTermInterface {

  java.util.ArrayList<QueryElement> queryTerms;
  Logger logger;

  public BannerJournalEntryQuery() {
    queryTerms = new java.util.ArrayList<QueryElement>();
    logger = LoggerFactory.getLogger(BannerJournalEntryQuery.class.getName());
  }

  @Override
  public void matchBranchId(org.osid.id.Id branchId, boolean match) {
    this.queryTerms.add(new QueryElement("branch", branchId.getIdentifier(), match, "Int"));
  }

  @Override
  public void clearBranchIdTerms() {
    this.clearTerms("branch");
  }

  @Override
  public void matchSourceId(org.osid.id.Id sourceId, boolean match) {
    this.queryTerms.add(new QueryElement("source", sourceId.getIdentifier(), match, "Int"));
  }

  @Override
  public void clearSourceIdTerms() {
    this.clearTerms("source");
  }

  @Override
  public void matchVersionId(org.osid.id.Id versionId, boolean match) {
    this.queryTerms.add(new QueryElement("version", versionId.getIdentifier(), match, "Int"));
  }

  @Override
  public void clearVersionIdTerms() {
    this.clearTerms("version");
  }

  @Override
  public void matchTimestamp(
      org.osid.calendaring.DateTime from, org.osid.calendaring.DateTime to, boolean match) {
    String fromString =
        String.format("%04d", from.getYear())
            + '-'
            + String.format("%02d", from.getMonth())
            + '-'
            + String.format("%02d", from.getDay());
    String toString =
        String.format("%04d", to.getYear())
            + '-'
            + String.format("%02d", to.getMonth())
            + '-'
            + String.format("%02d", to.getDay());
    this.queryTerms.add(new QueryElement("DateFrom", fromString, match, "DateFrom"));
    this.queryTerms.add(new QueryElement("DateTo", toString, match, "DateTo"));
  }

  @Override
  public void clearTimestampTerms() {
    this.clearTerms("DateFrom");
    this.clearTerms("DateTo");
  }

  @Override
  public void matchEntriesSince(org.osid.calendaring.DateTime from, boolean match) {
    String fromString =
        String.format("%04d", from.getYear())
            + '-'
            + String.format("%02d", from.getMonth())
            + '-'
            + String.format("%02d", from.getDay());
    this.queryTerms.add(new QueryElement("DateSince", fromString, match, "DateSince"));
  }

  @Override
  public void clearEntriesSinceTerms() {
    this.clearTerms("DateSince");
  }

  @Override
  public void matchResourceId(org.osid.id.Id resourceId, boolean match) {
    this.queryTerms.add(new QueryElement("resource", resourceId.getIdentifier(), match, "Int"));
  }

  @Override
  public void clearResourceIdTerms() {
    this.clearTerms("resource");
  }

  @Override
  public void matchAgentId(org.osid.id.Id agentId, boolean match) {
    this.queryTerms.add(new QueryElement("agent", agentId.getIdentifier(), match, "Int"));
  }

  @Override
  public void clearAgentIdTerms() {
    this.clearTerms("agent");
  }

  @Override
  public void matchJournalId(org.osid.id.Id journalId, boolean match) {
    logger.debug("matchJournalId Not implemented");
  }

  @Override
  public void clearJournalIdTerms() {
    logger.debug("clearJournalIdTerms Not implemented");
  }

  @Override
  public void clearGenusTypeTerms() {
    this.clearTerms("pt_id");
  }

  // TODO: GENUS_TYPE is no longer PT_ID
  @Override
  public void matchGenusType(org.osid.type.Type type, boolean match) {
    this.queryTerms.add(new QueryElement("pt_id", type.getIdentifier(), match, "String"));
  }

  @Override
  public void matchId(org.osid.id.Id id, boolean match) {
    this.queryTerms.add(new QueryElement("id", id.getIdentifier(), match, "String"));
  }

  private void clearTerms(String type) {
    for (int i = 0; i < this.queryTerms.size(); i++) {
      if (queryTerms.get(i).getType().equals(type)) {
        this.queryTerms.remove(i);
      }
    }
  }

  public List<QueryElement> getQueryTerms() {
    List<QueryElement> listQE = (List) this.queryTerms;
    return listQE;
  }
}
