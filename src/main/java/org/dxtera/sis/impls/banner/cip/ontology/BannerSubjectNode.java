package org.dxtera.sis.impls.banner.cip.ontology;

import java.util.ArrayList;
import java.util.List;
import net.okapia.osid.jamocha.ontology.subjectnode.ArraySubjectNodeList;
import org.osid.ontology.Subject;
import org.osid.ontology.SubjectNode;
import org.osid.ontology.SubjectNodeList;

/**
 * Bean version of Subject Node
 *
 * @author nwright
 */
public class BannerSubjectNode extends BannerNode<BannerSubjectNode> implements SubjectNode {

  private Subject subject;

  @Override
  public Subject getSubject() {
    return subject;
  }

  public void setSubject(Subject subject) {
    this.subject = subject;
    if (subject == null) {
      this.setId(null);
    } else {
      this.setId(subject.getId());
    }
  }

  @Override
  public SubjectNodeList getParentSubjectNodes() {
    List<SubjectNode> list = new ArrayList<>();
    list.addAll(this.getParentNodes().values());
    return new ArraySubjectNodeList(list);
  }

  @Override
  public SubjectNodeList getChildSubjectNodes() {
    List<SubjectNode> list = new ArrayList<>();
    list.addAll(this.getChildNodes().values());
    return new ArraySubjectNodeList(list);
  }
}
