package org.dxtera.sis.impls.banner.cip.ontology;

public interface AuthorityInterface {

  // String authority;

  public String getAuthority();

  public void setAuthority(String authority);
}
