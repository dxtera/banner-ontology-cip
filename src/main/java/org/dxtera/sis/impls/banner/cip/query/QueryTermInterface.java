package org.dxtera.sis.impls.banner.cip.query;

import java.util.List;

public interface QueryTermInterface {

  public List<QueryElement> getQueryTerms();
}
