package org.dxtera.sis.impls.banner.cip.ontology;

import net.okapia.osid.primordium.id.BasicId;
import org.dxtera.sis.impls.banner.cip.query.DatabaseController;
import org.osid.OperationFailedException;
import org.osid.binding.java.annotation.OSID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// The OntologyManager is provided to consumers via the
//   OSID runtime, and it is the entry point for obtaining
//   any OSID session for the ``ontology`` package.

// Scott Thorne (Templated by Jeff Merriman, Cole Shaw, Amon Horne)
// DXtera
// February 2017
//
// Copyright (c) 2017 DXtera Institute. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicense, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

// Typically we want to extend from net.okapia.osid.jamocha where possible,
// since that package contains a lot of helpful Abstract base
// classes and implementations.
// We always want to state which OSID interfaces we are implementing.
public class BannerOntologyManager
    extends net.okapia.osid.jamocha.ontology.spi.AbstractOntologyManager
    implements org.osid.ontology.OntologyManager, org.osid.ontology.OntologyProxyManager {

  // Set up a logger...
  protected final Logger logger = LoggerFactory.getLogger(BannerOntologyManager.class);

  // You can imagine a configuration value changing the "type"
  //   of Ontology that is sent into the sessions in
  //   initialize().
  public BannerOntology catalog;
  public String authority;

  protected DatabaseController dc;

  /** Constructs a new OntologyManager. */
  public BannerOntologyManager() throws OperationFailedException {
    // Depending on configuration (somehow in the constructor,
    //   and not in initialize?), you could imagine
    //   setting different field values in the ServiceProvider,
    //   so that it will report a school-variant.
    super(new BannerServiceProvider());
    // super(new ASUServiceProvider());
    return;
  }

  /**
   * Initializes this manager. A manager is initialized once at the time of creation.
   *
   * @param runtime the runtime environment
   * @throws org.osid.ConfigurationErrorException an error with implementation configuration
   * @throws org.osid.IllegalStateException this manager has already been initialized by the <code>
   *      OsidRuntime </code>
   * @throws org.osid.NullArgumentException <code> runtime </code> is <code> null </code>
   * @throws org.osid.OperationFailedException unable to complete request
   */
  // The @OSID annotation simple indicates that this is
  //   an OSID method.
  @OSID
  @Override
  public void initialize(org.osid.OsidRuntimeManager runtime)
      throws org.osid.ConfigurationErrorException, org.osid.OperationFailedException {

    super.initialize(runtime);

    org.osid.id.Id param1 = BasicId.valueOf("configuration:institution@local");

    this.authority = new String(getConfigurationValue(param1).getStringValue());

    this.dc = DatabaseController.newInstance("ontology");
    this.catalog = new BannerOntology(BasicId.valueOf("Ontology:1@" + this.authority));

    return;
  }

  public java.sql.Connection getConnection() throws org.osid.OperationFailedException {
    return (this.dc.getConnection());
  }

  // Each session that is accessible from this OntologyManager should
  //   include a constructor that takes a Ontology parameter.
  //   Here we're using the BannerBannerOntology, but
  //   with more impls, we can imagine sending different
  //   Ontologies to each session based on configuration
  //   or other criteria.
  // The @OSID annotation simple indicates that this is
  //   an OSID method.
  @OSID
  @Override
  // Note how the return value is defined by the ``org.osid``
  //   interface for a ``SubjectLookupSession``.
  public org.osid.ontology.SubjectLookupSession getSubjectLookupSession()
      throws org.osid.OperationFailedException {

    this.catalog = new BannerOntology(BasicId.valueOf("Ontology:1@" + this.authority));

    // You can imagine a configuration value changing the "type"
    //   of Ontology that is sent into the sessions.
    return new BannerSubjectLookupSession(this);
  }

  // The @OSID annotation simple indicates that this is
  //   an OSID method.
  @OSID
  @Override
  public boolean supportsSubjectLookup() {

    return (true);
  }

  // Each session that is accessible from this OntologyManager should
  //   include a constructor that takes a Ontology parameter.
  //   Here we're using the BannerBannerOntology, but
  //   with more impls, we can imagine sending different
  //   Ontologies to each session based on configuration
  //   or other criteria.
  // The @OSID annotation simple indicates that this is
  //   an OSID method.
  @OSID
  @Override
  // Note how the return value is defined by the ``org.osid``
  //   interface for a ``SubjectLookupSession``.
  public org.osid.ontology.SubjectLookupSession getSubjectLookupSessionForOntology(
      org.osid.id.Id ontologyId) throws org.osid.OperationFailedException {

    this.catalog = new BannerOntology(ontologyId);
    return new BannerSubjectLookupSession(this);
  }

  // Each session that is accessible from this OntologyManager should
  //   include a constructor that takes a Ontology parameter.
  //   Here we're using the BannerBannerOntology, but
  //   with more impls, we can imagine sending different
  //   Ontologies to each session based on configuration
  //   or other criteria.
  // The @OSID annotation simple indicates that this is
  //   an OSID method.
  @OSID
  @Override
  // Note how the return value is defined by the ``org.osid``
  //   interface for a ``RelevancyLookupSession``.
  public org.osid.ontology.RelevancyLookupSession getRelevancyLookupSession()
      throws org.osid.OperationFailedException {

    this.catalog = new BannerOntology(BasicId.valueOf("Ontology:1@" + this.authority));

    // You can imagine a configuration value changing the "type"
    //   of Ontology that is sent into the sessions.
    return new BannerRelevancyLookupSession(this);
  }

  // The @OSID annotation simple indicates that this is
  //   an OSID method.
  @OSID
  @Override
  public boolean supportsRelevancyLookup() {

    return (true);
  }

  // Each session that is accessible from this OntologyManager should
  //   include a constructor that takes a Ontology parameter.
  //   Here we're using the BannerBannerOntology, but
  //   with more impls, we can imagine sending different
  //   Ontologies to each session based on configuration
  //   or other criteria.
  // The @OSID annotation simple indicates that this is
  //   an OSID method.
  @OSID
  @Override
  // Note how the return value is defined by the ``org.osid``
  //   interface for a ``RelevancyLookupSession``.
  public org.osid.ontology.RelevancyLookupSession getRelevancyLookupSessionForOntology(
      org.osid.id.Id ontologyId) throws org.osid.OperationFailedException {

    this.catalog = new BannerOntology(ontologyId);
    return new BannerRelevancyLookupSession(this);
  }

  // Each session that is accessible from this OntologyManager should
  //   include a constructor that takes a Ontology parameter.
  //   Here we're using the BannerBannerOntology, but
  //   with more impls, we can imagine sending different
  //   Ontologies to each session based on configuration
  //   or other criteria.
  // The @OSID annotation simple indicates that this is
  //   an OSID method.
  @OSID
  @Override
  // Note how the return value is defined by the ``org.osid``
  //   interface for a ``RelevancyQuerySession``.
  public org.osid.ontology.RelevancyQuerySession getRelevancyQuerySession()
      throws org.osid.OperationFailedException {

    this.catalog = new BannerOntology(BasicId.valueOf("Ontology:1@" + this.authority));

    // You can imagine a configuration value changing the "type"
    //   of Ontology that is sent into the sessions.
    return new BannerRelevancyQuerySession(this);
  }

  // The @OSID annotation simple indicates that this is
  //   an OSID method.
  @OSID
  @Override
  public boolean supportsRelevancyQuery() {

    return (true);
  }

  // Each session that is accessible from this OntologyManager should
  //   include a constructor that takes a Ontology parameter.
  //   Here we're using the BannerBannerOntology, but
  //   with more impls, we can imagine sending different
  //   Ontologies to each session based on configuration
  //   or other criteria.
  // The @OSID annotation simple indicates that this is
  //   an OSID method.
  @OSID
  @Override
  // Note how the return value is defined by the ``org.osid``
  //   interface for a ``RelevancyQuerySession``.
  public org.osid.ontology.RelevancyQuerySession getRelevancyQuerySessionForOntology(
      org.osid.id.Id ontologyId) throws org.osid.OperationFailedException {

    this.catalog = new BannerOntology(ontologyId);
    return new BannerRelevancyQuerySession(this);
  }

  // Each session that is accessible from this OntologyManager should
  //   include a constructor that takes a Ontology parameter.
  //   Here we're using the BannerBannerOntology, but
  //   with more impls, we can imagine sending different
  //   Ontologies to each session based on configuration
  //   or other criteria.
  // The @OSID annotation simple indicates that this is
  //   an OSID method.
  @OSID
  @Override
  // Note how the return value is defined by the ``org.osid``
  //   interface for a ``SubjectHierarchySession``.
  public org.osid.ontology.SubjectHierarchySession getSubjectHierarchySession()
      throws org.osid.OperationFailedException {

    this.catalog = new BannerOntology(BasicId.valueOf("Ontology:1@" + this.authority));

    // You can imagine a configuration value changing the "type"
    //   of Ontology that is sent into the sessions.
    return new BannerSubjectHierarchySession(this);
  }

  public org.osid.ontology.SubjectHierarchySession getSubjectHierarchySessionForOntology(org.osid.id.Id ontId)
      throws org.osid.OperationFailedException {

    this.catalog = new BannerOntology(ontId);

    // You can imagine a configuration value changing the "type"
    //   of Ontology that is sent into the sessions.
    return new BannerSubjectHierarchySession(this);
  }

  // The @OSID annotation simple indicates that this is
  //   an OSID method.
  @OSID
  @Override
  public boolean supportsSubjectHierarchy() {

    return (true);
  }
}
