package org.dxtera.sis.impls.banner.cip.ontology;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import net.okapia.osid.jamocha.hierarchy.node.ArrayNodeList;
import net.okapia.osid.jamocha.id.id.ArrayIdList;
import org.osid.hierarchy.Node;
import org.osid.hierarchy.NodeList;
import org.osid.id.Id;
import org.osid.id.IdList;

/**
 * Bean version of node
 *
 * @author nwright
 */
public class BannerNode<E extends BannerNode> implements Node {

  private Id id;
  private Map<Id, E> parentNodes = new LinkedHashMap<>();
  private boolean hasParents = false;
  private Map<Id, E> childNodes = new LinkedHashMap<>();
  private boolean hasChildren = false;

  ////
  //// bean properties
  ////
  @Override
  public Id getId() {
    return id;
  }

  public void setId(Id id) {
    this.id = id;
  }

  public Map<Id, E> getParentNodes() {
    return parentNodes;
  }

  public void setParentNodes(Map<Id, E> parentNodes) {
    this.parentNodes = parentNodes;
  }

  public Map<Id, E> getChildNodes() {
    return childNodes;
  }

  public void setChildNodes(Map<Id, E> childNodes) {
    this.childNodes = childNodes;
  }

  public boolean isHasParents() {
    return hasParents;
  }

  public void setHasParents(boolean hasParents) {
    this.hasParents = hasParents;
  }

  public boolean isHasChildren() {
    return hasChildren;
  }

  public void setHasChildren(boolean hasChildren) {
    this.hasChildren = hasChildren;
  }

  @Override
  public NodeList getParents() {
    List<Node> list = new ArrayList<>();
    for (BannerNode child : this.parentNodes.values()) {
      list.add(child);
    }
    return new ArrayNodeList(list);
  }

  @Override
  public NodeList getChildren() {
    List<Node> list = new ArrayList<>();
    for (BannerNode child : this.childNodes.values()) {
      list.add(child);
    }
    return new ArrayNodeList(list);
  }

  @Override
  public boolean isRoot() {
    if (this.hasParents()) {
      return false;
    }
    return true;
  }

  @Override
  public boolean hasParents() {
    // can have parents but not know who they are
    if (this.hasParents) {
      return true;
    }
    if (this.parentNodes.isEmpty()) {
      return false;
    }
    return true;
  }

  @Override
  public IdList getParentIds() {
    List<Id> list = new ArrayList<>();
    for (BannerNode child : this.parentNodes.values()) {
      list.add(child.getId());
    }
    return new ArrayIdList(list);
  }

  @Override
  public boolean isLeaf() {
    if (this.hasChildren()) {
      return false;
    }
    return true;
  }

  @Override
  public boolean hasChildren() {
    // can have children but not know who they are
    if (hasChildren) {
      return true;
    }
    if (this.childNodes.isEmpty()) {
      return false;
    }
    return true;
  }

  @Override
  public IdList getChildIds() {
    List<Id> list = new ArrayList<>();
    for (BannerNode child : this.childNodes.values()) {
      list.add(child.getId());
    }
    return new ArrayIdList(list);
  }

  @Override
  public boolean isCurrent() {
    return false;
  }

  @Override
  public boolean isSequestered() {
    return false;
  }
}
