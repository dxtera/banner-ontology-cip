package org.dxtera.sis.impls.banner.cip.ontology;

import static net.okapia.osid.primordium.locale.text.eng.us.Plain.text;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.InputStream;
import java.sql.ResultSet;
import java.util.HashMap;
import net.okapia.osid.jamocha.builder.ontology.subject.MutableSubject;
import net.okapia.osid.primordium.calendaring.GregorianUTCDateTime;
import net.okapia.osid.primordium.id.BasicId;
import net.okapia.osid.primordium.type.BasicType;
import org.dxtera.sis.impls.banner.cip.ontology.type.TypeConstants;
import org.dxtera.utils.database.postgres.ResultSetUtils;
import org.osid.OperationFailedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// This class is the ``Relevancy`` entity. Until we
//   find out otherwise, we'll use a single Relevancy
//   for all the ontology implementations.
// If we find out that there are Relevancy variants,
//   then we may need to create implementation-specific
//   Relevancy classes.

// Scott Thorne, Cole Shaw, Amon Horne
// Okapia
// February 2017
//
// Copyright (c) 2017 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicense, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// We want to extend from jamocha when we can, since it provides
// a lot of built-in functionality.
// We always want to state which OSID interfaces we are implementing.
// We also implement the Generator interface so that
// we can use the protected setters from
// jamocha.
public class BannerRelevancy
    extends net.okapia.osid.jamocha.ontology.relevancy.spi.AbstractRelevancy
    implements org.osid.ontology.Relevancy, RelevancyGeneratorInterface, AuthorityInterface {

  protected String authority;

  public String getAuthority() {

    return this.authority;
  }

  public void setAuthority(String authority) {
    this.authority = authority;
  }

  public BannerRelevancy(String authority) throws OperationFailedException {
    super();
    this.authority = authority;
  }

  public BannerRelevancy() throws OperationFailedException {
    super();
    this.authority = "dxtera";
  }

  public HashMap<String, String> getMapFile(String fileName) {
    StringBuilder filePath = new StringBuilder();
    String basePath = "cip/map/ontology/" + fileName;
    String extPath = "cip/map/" + authority + "/ontology/" + fileName;
    HashMap<String, String> jsonMap = new HashMap<>();
    try {
      ObjectMapper objectMapper = new ObjectMapper();
      InputStream input =
          (Thread.currentThread().getContextClassLoader().getResourceAsStream(extPath.toString())
                  != null)
              ? Thread.currentThread()
                  .getContextClassLoader()
                  .getResourceAsStream(extPath.toString())
              : Thread.currentThread()
                  .getContextClassLoader()
                  .getResourceAsStream(basePath.toString());
      jsonMap = objectMapper.readValue(input, new TypeReference<HashMap<String, String>>() {});
    } catch (Exception e) {
      e.printStackTrace();
    }
    return jsonMap;
  }

  // This method takes an SQL ResultSet and converts
  //   it into a Relevancy object.
  @Override
  public org.osid.ontology.Relevancy convert(ResultSet rs) throws OperationFailedException {

    HashMap<String, String> typeMap = this.getMapFile("relevancyfile.json");
    Logger logger = LoggerFactory.getLogger(BannerRelevancy.class);

    current();

    // Note that we use our package's Relevancy object
    //   so that we can access the setter methods.
    BannerRelevancy relevancy = new BannerRelevancy(this.authority);

    // You can set values using the setters for our impl
    //   and pull values from the ResultSet.
    // Here is where you would modify the column names to correctly map to
    //   the Banner implementation.
    try {
      relevancy.setId(
          new net.okapia.osid.primordium.id.BasicId(
              this.authority, "Relevancy", rs.getString("id")));
      relevancy.setDisplayName(text(ResultSetUtils.getString(rs.getString("display_name"))));
      relevancy.setDescription(text(ResultSetUtils.getString(rs.getString("description"))));

      if (typeMap.get(rs.getString("pt_id")) == null) {
        logger.info("ERR - Missing Relevancy Type for: " + rs.getString("pt_id"));
      }
      relevancy.setGenusType(
          new BasicType(
              this.authority,
              "Relevancy",
              typeMap.getOrDefault(rs.getString("pt_id"), rs.getString("pt_id"))));

      // If Banner requires special handling of any field types, you can create
      //   a Utility class that differs from our PostgreSQL version.

      MutableSubject subject = new MutableSubject();
      subject.setId(
          new net.okapia.osid.primordium.id.BasicId(
              this.authority, "Subject", rs.getString("subject")));
      relevancy.setSubject(subject);
      relevancy.setMappedId(BasicId.valueOf(rs.getString("mapped")));
      if (rs.getDate("start_date") != null) {
        relevancy.setStartDate(GregorianUTCDateTime.valueOf(rs.getDate("start_date")));
      } else {
        relevancy.setStartDate(TypeConstants.BEGINNING_OF_TIME);
      }
      if (rs.getDate("end_date") != null) {
        relevancy.setEndDate(GregorianUTCDateTime.valueOf(rs.getDate("end_date")));
      } else {
        relevancy.setEndDate(TypeConstants.END_OF_TIME);
      }

      // Once we support record types, you should also check
      //   and call addRecord() here.

      // if (hasRecordType(BasicType.valueOf(rs.getString("genus_type")))) {
      //     addRecord(getTermRecord(BasicType.valueOf(rs.getString("genus_type"))));
      // }
    } catch (java.sql.SQLException e) {
      // Do something here
    }

    return relevancy;
  }

  /**
   * This method copies an "unknown" provider Relevancy into a Relevancy object from this package.
   * This is often helpful to ensure that a Relevancy behaves as expected.
   */
  public org.osid.ontology.Relevancy convert(org.osid.ontology.Relevancy other)
      throws OperationFailedException {

    // Note that we use our package's Relevancy object
    //   so that we can access the setter methods.
    BannerRelevancy relevancy = new BannerRelevancy(this.authority);

    relevancy.setId(other.getId());
    relevancy.setDescription(other.getDescription());
    relevancy.setDisplayName(other.getDisplayName());
    relevancy.setGenusType(other.getGenusType());

    MutableSubject subject = new MutableSubject();
    subject.setId(other.getSubjectId());
    relevancy.setSubject(subject);
    relevancy.setMappedId(BasicId.valueOf(other.getMappedId()));
    relevancy.setStartDate(other.getStartDate());
    relevancy.setEndDate(other.getEndDate());

    // Once we support record types, you should also check
    //   and call addRecord() here.

    // if (hasRecordType(other.getGenusType())) {
    //     addRecord(other.getTermRecord(other.getGenusType()));
    // }

    return relevancy;
  }

  // If you need to, you can override the default Jamocha
  // behavior for this method.
  // @Override
  // public org.osid.id.Id getSubjectId() {
  //  return super.getSubjectId();
  // }

  // If you need to, you can override the default Jamocha
  // behavior for this method.
  // @Override
  // protected void setSubject(org.osid.id.Id id) {
  //   super.setSubject(id);
  // }

  // If you need to, you can override the default Jamocha
  // behavior for this method.
  // @Override
  // public org.osid.ontology.Subject getSubject()
  //    throws OperationFailedException {
  //
  //  return super.getSubject();
  // }

  // If you need to, you can override the default Jamocha
  // behavior for this method.
  // @Override
  // public org.osid.id.Id getMappedId() {
  //  return super.getMappedId();
  // }

  // If you need to, you can override the default Jamocha
  // behavior for this method.
  // @Override
  // protected void setMapped(org.osid.id.Id id) {
  //   super.setMapped(id);
  // }
}
