package org.dxtera.sis.impls.banner.cip.ontology.journaling.relevancy;

import java.util.ArrayList;
import net.okapia.osid.jamocha.journaling.journalentry.ArrayJournalEntryList;
import org.osid.NotFoundException;
import org.osid.binding.java.annotation.OSID;

// Scott Thorne (Templated by Jeff Merriman, Cole Shaw, Amon Horne)
// DXtera
// February 2017
//
// Copyright (c) 2017 DXtera Institute. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicense, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

// Typically we want to extend from net.okapia.osid.jamocha where possible,
// since that package contains a lot of helpful Abstract base
// classes and implementations.
// We always want to state which OSID interfaces we are implementing.
public class BannerJournalEntryLookupSession
    extends net.okapia.osid.jamocha.journaling.spi.AbstractJournalEntryLookupSession
    implements org.osid.journaling.JournalEntryLookupSession {

  protected BannerJournalInterface catalog;

  // This is the JournalEntryQuerySession that this JournalEntryLookupSession
  //    will be built upon.
  protected org.osid.journaling.JournalEntryQuerySession querySession;
  protected BannerJournalingManager mgr;

  // We use our BannerJournalInterface for the catalog
  //   type here, instead of the generic OSID Journal
  //   interface, since we know these implementations are tied
  //   together.
  public BannerJournalEntryLookupSession(BannerJournalingManager mgr)
      throws org.osid.OperationFailedException {

    super();
    this.mgr = mgr;
    setJournal(mgr.catalog);
    this.catalog = mgr.catalog;
    this.querySession = new BannerJournalEntryQuerySession(mgr);
    return;
  }

  @Override
  public boolean canReadJournal() {
    return true;
  }

  // We override the Jamocha impl of getJournalEntry(),
  //   typically for performance reasons. Jamocha iterates through
  //   all the JournalEntries to find an Id match, and
  //   using direct SQL queries we know that we can have a
  //   more performant impl.
  // The @OSID annotation simple indicates that this is
  //   an OSID method.
  @OSID
  @Override
  public org.osid.journaling.JournalEntry getJournalEntry(org.osid.id.Id journalEntryId)
      throws org.osid.OperationFailedException, org.osid.PermissionDeniedException,
          org.osid.NotFoundException {

    // Here we grab a JournalEntryQuery and use it to
    //   do lookup. It does assume certain methods
    //   are implemented in the Query and the QuerySession.

    // Note that we tend to provide fully qualified
    //   class paths inline, instead of importing
    //   at the top of the file and calling just
    //   "JournalEntry" here. That way we avoid any
    //   confusion with the package's JournalEntry class
    //   and the org.osid JournalEntry interface.
    org.osid.journaling.JournalEntryQuery query = this.querySession.getJournalEntryQuery();
    query.matchId(journalEntryId, true);
    org.osid.journaling.JournalEntryList results =
        this.querySession.getJournalEntriesByQuery(query);
    org.osid.journaling.JournalEntry obj;
    if (results.hasNext()) {
      obj = results.getNextJournalEntry();
    } else {
      throw new NotFoundException("Could not find the JournalEntry: " + journalEntryId);
    }
    results.close();
    results = null;
    return obj;
  }

  // If we do not need to override the Jamocha implementation, then
  //    we can leave these methods out or just call super().
  @Override
  public org.osid.journaling.JournalEntryList getJournalEntriesByIds(
      org.osid.id.IdList journalEntryIds)
      throws org.osid.NotFoundException, org.osid.OperationFailedException,
          org.osid.PermissionDeniedException {

    // NOTE: Every passed in Id must return at most one
    //       unique entity (or none). Each given Id should
    //       NEVER refer to multiple rows in the system of record.
    // NOTE: Per the spec, Ids can be repeated in the arguments,
    //       which may result in duplicate results returned
    //       (depending on plenary vs. comparative).
    // Read this wiki article for more information:
    //    https://dxtera.atlassian.net/wiki/spaces/TEC/pages/250806281/Sanity+With+Ids

    org.osid.journaling.JournalEntryQuery query = this.querySession.getJournalEntryQuery();

    while (journalEntryIds.hasNext()) {
      query.matchId(journalEntryIds.getNextId(), true);
    }

    org.osid.journaling.JournalEntryList results =
        this.querySession.getJournalEntriesByQuery(query);
    if (results.hasNext()) {
      return results;
    }
    return new ArrayJournalEntryList(new ArrayList<org.osid.journaling.JournalEntry>());
  }

  // If we do not need to override the Jamocha implementation, then
  //    we can leave these methods out or just call super().
  @Override
  public org.osid.journaling.JournalEntryList getJournalEntriesByGenusType(org.osid.type.Type type)
      throws org.osid.OperationFailedException, org.osid.PermissionDeniedException {

    org.osid.journaling.JournalEntryQuery query = this.querySession.getJournalEntryQuery();

    query.matchGenusType(type, true);

    org.osid.journaling.JournalEntryList results =
        this.querySession.getJournalEntriesByQuery(query);
    if (results.hasNext()) {
      return results;
    }
    return new ArrayJournalEntryList(new ArrayList<org.osid.journaling.JournalEntry>());
  }

  // If we do not need to override the Jamocha implementation, then
  //    we can leave these methods out or just call super().
  // @Override
  // public org.osid.journaling.JournalEntryList
  // getJournalEntriesByParentGenusType(org.osid.type.Type journalEntryGenusType)
  //     throws org.osid.OperationFailedException,
  //            org.osid.PermissionDeniedException {

  //   return super.getJournalEntriesByParentGenusType(journalEntryGenusType);
  // }

  // If we do not need to override the Jamocha implementation, then
  //    we can leave these methods out or just call super().
  // @Override
  // public org.osid.journaling.JournalEntryList getJournalEntriesByRecordType(org.osid.type.Type
  // journalEntryRecordType)
  //     throws org.osid.OperationFailedException,
  //            org.osid.PermissionDeniedException {

  //   return super.getJournalEntriesByRecordType(journalEntryRecordType);
  // }

  @Override
  public org.osid.journaling.JournalEntryList getJournalEntriesByDateForBranch(
      org.osid.id.Id branchId, org.osid.calendaring.DateTime d)
      throws org.osid.OperationFailedException, org.osid.PermissionDeniedException {

    org.osid.journaling.JournalEntryQuery query = this.querySession.getJournalEntryQuery();
    query.matchEntriesSince(d, true);
    org.osid.journaling.JournalEntryList results =
        this.querySession.getJournalEntriesByQuery(query);

    if (results.hasNext()) {
      return results;
    }

    return new ArrayJournalEntryList(new ArrayList<org.osid.journaling.JournalEntry>());
  }

  // This is the only required method impl to get the
  //   abstract Jamocha LookupSessions to work.
  // The @OSID annotation simple indicates that this is
  //   an OSID method.
  @OSID
  @Override
  // The JournalEntryList that is returned should be an iterator so that
  //   we don't try to unpack all the results into memory.
  public org.osid.journaling.JournalEntryList getJournalEntries()
      throws org.osid.OperationFailedException, org.osid.PermissionDeniedException {

    // Here we'll need the Banner-specific query to get all the
    //   JournalEntry entities.
    // The code below is from a Postgres implementation, so change
    //   it to match Banner syntax as needed.
    // You can also account for school-specific variants here by
    //   fetching data from the `Journal` and injecting
    //   it into the SELECT statement.

    // Here we avoid the Jamocha JDBC List iterators because they seem
    //   to hang if the buffer size is set too small relative to
    //   the number of rows. We use the SIS impls instead, or you
    //   can create your own iterators.

    org.osid.journaling.JournalEntryQuery query = this.querySession.getJournalEntryQuery();

    org.osid.journaling.JournalEntryList results =
        this.querySession.getJournalEntriesByQuery(query);
    if (results.hasNext()) {
      return results;
    }
    return new ArrayJournalEntryList(new ArrayList<org.osid.journaling.JournalEntry>());
  }
}
