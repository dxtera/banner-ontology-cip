/**
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * <p>http://www.apache.org/licenses/LICENSE-2.0
 *
 * <p>Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.dxtera.sis.impls.banner.csv.beans;

/**
 * See http://nces.ed.gov/ipeds/cipcode/resources.aspx?y=55
 *
 * @author nwright
 */
public class CipBean {

  private String cipFamily;
  private String cipCode;
  private String action;
  private boolean textChange;
  private String cipTitle;
  private String cipDefinition;
  private String cipCrossreferences;
  private String examples;

  public CipBean() {}

  public String getCipFamily() {
    return cipFamily;
  }

  public void setCipFamily(String cipFamily) {
    this.cipFamily = cipFamily;
  }

  public String getCipCode() {
    return cipCode;
  }

  public void setCipCode(String cipCode) {
    this.cipCode = cipCode;
  }

  public String getAction() {
    return action;
  }

  public void setAction(String action) {
    this.action = action;
  }

  public boolean isTextChange() {
    return textChange;
  }

  public void setTextChange(boolean textChange) {
    this.textChange = textChange;
  }

  public String getCipTitle() {
    return cipTitle;
  }

  public void setCipTitle(String cipTitle) {
    this.cipTitle = cipTitle;
  }

  public String getCipDefinition() {
    return cipDefinition;
  }

  public void setCipDefinition(String cipDefinition) {
    this.cipDefinition = cipDefinition;
  }

  public String getCipCrossreferences() {
    return cipCrossreferences;
  }

  public void setCipCrossreferences(String cipCrossreferences) {
    this.cipCrossreferences = cipCrossreferences;
  }

  public String getExamples() {
    return examples;
  }

  public void setExamples(String examples) {
    this.examples = examples;
  }
}
