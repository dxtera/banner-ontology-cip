package org.dxtera.sis.impls.banner.cip.ontology;

// Scott Thorne templated by Jeff Merriman, Cole Shaw, Amon Horne
// DXtera
// October 2017
//
// Copyright (c) 2017 DXtera Institute. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicense, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
public final class BannerServiceProvider
    extends net.okapia.osid.provider.spi.AbstractServiceProvider
    implements net.okapia.osid.provider.ServiceProvider {

  // ID, DISPLAY_NAME, and DESCRIPTION values should be consistent for
  //   the package and not change regularly.
  // You can also imagine extending this class for a school-specific ServiceProvider,
  //   that includes school information in the DISPLAY_NAME or DESCRIPTION.
  protected static final String ID = "urn:osid:dxtera.org:identifiers:sis:impls:Banner:Ontology";
  protected static final String DISPLAY_NAME = "Banner Ontology Provider";
  protected static final String DESCRIPTION = "Banner Ontology Provider";

  // As you update the code and issue new releases, you should also
  //   update the VERSION and RELEASE_DATE values.
  protected static final String VERSION = "0.1.0";
  protected static final String RELEASE_DATE = "2019-02-04";

  protected BannerServiceProvider() {
    setServiceId(ID);
    setServiceName(net.okapia.osid.primordium.locale.text.eng.us.Plain.valueOf(DISPLAY_NAME));
    setServiceDescription(net.okapia.osid.primordium.locale.text.eng.us.Plain.valueOf(DESCRIPTION));
    setImplementationVersion(VERSION);
    setReleaseDate(RELEASE_DATE);
    setProvider(net.okapia.osid.provider.Providers.OKAPIA.getProvider());
    return;
  }
}
