package org.dxtera.sis.impls.banner.cip.ontology.journaling.relevancy;

import java.util.ArrayList;
import net.okapia.osid.jamocha.builder.journaling.journal.MutableJournal;
import net.okapia.osid.jamocha.journaling.journal.ArrayJournalList;
import org.dxtera.sis.impls.banner.cip.ontology.journaling.JournalGeneratorInterface;
import org.osid.NotFoundException;
import org.osid.OperationFailedException;
import org.osid.PermissionDeniedException;
import org.osid.binding.java.annotation.OSID;
import org.osid.id.Id;
import org.osid.journaling.Journal;

// by-hand
//
// Scott Thorne (Templated by Jeff Merriman, Cole Shaw, Amon Horne)
// DXtera
// February 2017
//
// Copyright (c) 2017 DXtera Institute. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicense, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

// Typically we want to extend from net.okapia.osid.jamocha where possible,
// since that package contains a lot of helpful Abstract base
// classes and implementations.
// We always want to state which OSID interfaces we are implementing.
public class BannerJournalLookupSession
    extends net.okapia.osid.jamocha.journaling.spi.AbstractJournalLookupSession
    implements org.osid.journaling.JournalLookupSession {

  private BannerJournalInterface catalog;
  private JournalGeneratorInterface generator;
  private String authority;
  private BannerJournalingManager mgr;

  ArrayList<Journal> listOfJournals = new ArrayList<Journal>();

  // This is the JournalEntryQuerySession that this JournalEntryLookupSession
  //    will be built upon.
  private org.osid.journaling.JournalEntryQuerySession querySession;

  // We use our BannerJournalInterface for the catalog
  //   type here, instead of the generic OSID Journal
  //   interface, since we know these implementations are tied
  //   together.
  protected BannerJournalLookupSession(BannerJournalingManager mgr)
      throws org.osid.OperationFailedException {

    super();
    this.mgr = mgr;
    this.authority = mgr.authority;
    this.catalog = mgr.catalog;

    listOfJournals.add((Journal) this.catalog);
    return;
  }

  protected BannerJournalLookupSession() throws org.osid.OperationFailedException {

    super();
    this.authority = "dxtera";

    listOfJournals.add((Journal) this.catalog);
    return;
  }

  @Override
  public boolean canLookupJournals() {
    return true;
  }

  // by-hand
  @Override
  public org.osid.journaling.Journal getJournal(org.osid.id.Id journalId)
      throws OperationFailedException, PermissionDeniedException, NotFoundException {
    MutableJournal myJournal = new MutableJournal();
    for (Journal j : listOfJournals) {
      if (j.getId().getIdentifier().equals(journalId.getIdentifier())) {
        myJournal.setId(
            new net.okapia.osid.primordium.id.BasicId(
                this.authority, "Journal", journalId.getIdentifier()));
      }
    }
    return (Journal) myJournal;
  }

  // by-hand
  @Override
  public org.osid.journaling.JournalList getJournalsByIds(org.osid.id.IdList journalIds)
      throws org.osid.NotFoundException, org.osid.OperationFailedException,
          org.osid.PermissionDeniedException {

    // NOTE: Every passed in Id must return at most one
    //       unique entity (or none). Each given Id should
    //       NEVER refer to multiple rows in the system of record.
    // NOTE: Per the spec, Ids can be repeated in the arguments,
    //       which may result in duplicate results returned
    //       (depending on plenary vs. comparative).
    // Read this wiki article for more information:
    //    https://dxtera.atlassian.net/wiki/spaces/TEC/pages/250806281/Sanity+With+Ids

    ArrayList<Journal> journalArray = new ArrayList<org.osid.journaling.Journal>();

    for (Journal j : listOfJournals) {
      while (journalIds.hasNext()) {
        Id i = journalIds.getNextId();
        if (j.getId().getIdentifier().equals(i.getIdentifier())) {
          journalArray.add(j);
        }
      }
    }

    return new ArrayJournalList(journalArray);
  }

  // by-hand
  @OSID
  @Override
  // The JournalEntryList that is returned should be an iterator so that
  //   we don't try to unpack all the results into memory.
  // @TODO: Yes, we should build this one out.
  public org.osid.journaling.JournalList getJournals()
      throws org.osid.OperationFailedException, org.osid.PermissionDeniedException {

    return new ArrayJournalList(listOfJournals);
  }
}
