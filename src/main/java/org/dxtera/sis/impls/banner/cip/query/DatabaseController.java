package org.dxtera.sis.impls.banner.cip.query;

import java.util.logging.Logger;
import java.sql.Statement;

public final class DatabaseController {
  private static DatabaseController DC;
  private final String DBCLASS;

  private final String CONFIG_FILE;
  private final com.zaxxer.hikari.HikariDataSource dataSource;
  Logger logger = Logger.getLogger(DatabaseController.class.getName());

  private DatabaseController(String package_name)
      throws org.osid.ConfigurationErrorException, org.osid.OperationFailedException {

    this.CONFIG_FILE = package_name + "_dbconfig.properties";

    try (java.io.InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream(CONFIG_FILE)) {
      if (is == null) {
        throw new org.osid.ConfigurationErrorException("cannot find " + CONFIG_FILE + " for database");
      }
      java.util.Properties properties = new java.util.Properties();
      properties.load(is);

      com.zaxxer.hikari.HikariConfig config = new com.zaxxer.hikari.HikariConfig();

      // bufferSize = Integer.valueOf(prop.getProperty("buffersize"));

      logger.info(properties.getProperty("dbclass"));
      logger.info(properties.getProperty("dburl"));
      config.setJdbcUrl(properties.getProperty("dburl"));
      config.setUsername(properties.getProperty("dbuser"));
      config.setPassword(properties.getProperty("dbpassword"));
      //config.setReadOnly(true);
      config.setAutoCommit(false);
      config.setMaximumPoolSize(10);
      switch (properties.getProperty("dbclass")) {
        case "com.impossibl.postgres.jdbc.PGDataSource":
          config.addDataSourceProperty("parsed-sql.cache.size", "256");
          config.addDataSourceProperty("fetch.size", "50");
          config.addDataSourceProperty("protocol.io.mode", "native");
          config.addDataSourceProperty("protocol.io.threads", "5");
          break;
        case "org.postgresql.ds.PGSimpleDataSource":
          config.addDataSourceProperty("defaultRowFetchSize", "50");
          config.addDataSourceProperty("preferQueryMode", "extendedCacheEverything");
          config.addDataSourceProperty("disableColumnSanitiser", "true");
          break;
        case "oracle.jdbc.pool.OracleDataSource":
          config.addDataSourceProperty("dataSourceClassName", "oracle.jdbc.pool.OracleDataSource");
          config.addDataSourceProperty("implicitCachingEnabled", "false");
          config.addDataSourceProperty("defaultRowPrefetch", "2000");
          config.addDataSourceProperty("defaultBatchValue", "2000");
          config.addDataSourceProperty("maxStatements", "10000");
          config.addDataSourceProperty("oracle.jdbc.implicitStatementCacheSize", "200");
          config.addDataSourceProperty("setStatementCacheSize", "200");
          break;
      }
      this.DBCLASS = properties.getProperty("dbclass");
      this.dataSource = new com.zaxxer.hikari.HikariDataSource(config);
    } catch (java.io.IOException ioe) {
      throw new org.osid.OperationFailedException(ioe);
    }
  }

  public static DatabaseController newInstance(String package_name)
      throws org.osid.ConfigurationErrorException, org.osid.OperationFailedException {

    if (DatabaseController.DC == null) {
      DatabaseController.DC = new DatabaseController(package_name);
    }
    return (DatabaseController.DC);
  }

  public java.sql.Connection getConnection() throws org.osid.OperationFailedException {

    try {
      java.sql.Connection c =  this.dataSource.getConnection();
         if (!this.DBCLASS.equals("oracle.jdbc.pool.OracleDataSource")) {
                logger.info("SQLSTANDARD TEMP TABLE CALL");
      Statement s = c.createStatement();
      String sql;
      sql = " drop table if exists ont_id; ";
      sql += " create temp table ont_id (id text); ";
      s.execute(sql);
      c.commit();
         }
      return (c);
    } catch (java.sql.SQLException sqe) {
      throw new org.osid.OperationFailedException(sqe);
    }
  }
}
