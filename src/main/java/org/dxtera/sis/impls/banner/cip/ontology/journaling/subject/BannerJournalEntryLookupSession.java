package org.dxtera.sis.impls.banner.cip.ontology.journaling.subject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import net.okapia.osid.jamocha.journaling.journalentry.ArrayJournalEntryList;
import org.dxtera.sis.impls.banner.csv.CsvService;
import org.dxtera.sis.impls.banner.csv.beans.CipBean;
import org.osid.NotFoundException;
import org.osid.OperationFailedException;
import org.osid.binding.java.annotation.OSID;
import org.osid.id.Id;
import org.osid.journaling.JournalEntry;

// Scott Thorne (Templated by Jeff Merriman, Cole Shaw, Amon Horne)
// DXtera
// February 2017
//
// Copyright (c) 2017 DXtera Institute. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicense, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

// Typically we want to extend from net.okapia.osid.jamocha where possible,
// since that package contains a lot of helpful Abstract base
// classes and implementations.
// We always want to state which OSID interfaces we are implementing.
public class BannerJournalEntryLookupSession
    extends net.okapia.osid.jamocha.journaling.spi.AbstractJournalEntryLookupSession
    implements org.osid.journaling.JournalEntryLookupSession {

  protected BannerJournalInterface catalog;
  protected BannerJournalingManager mgr;
  private List<JournalEntry> allJournalEntries;

  // We use our BannerJournalInterface for the catalog
  //   type here, instead of the generic OSID Journal
  //   interface, since we know these implementations are tied
  //   together.
  public BannerJournalEntryLookupSession(BannerJournalingManager mgr)
      throws OperationFailedException {

    super();
    this.mgr = mgr;
    setJournal(mgr.catalog);
    this.catalog = mgr.catalog;
    CsvService csvService = new CsvService();
    List<CipBean> cipCodes = csvService.getCIPCodes();
    this.allJournalEntries =
        cipCodes
            .stream()
            .map(
                naicsBean -> {
                  try {
                    return this.catalog.getJournalEntryGenerator().convert(naicsBean);
                  } catch (OperationFailedException e) {
                    e.printStackTrace();
                    return null;
                  }
                })
            .filter(Objects::nonNull)
            .collect(Collectors.toList());
    return;
  }

  @Override
  public boolean canReadJournal() {
    return true;
  }

  // We override the Jamocha impl of getJournalEntry(),
  //   typically for performance reasons. Jamocha iterates through
  //   all the JournalEntries to find an Id match, and
  //   using direct SQL queries we know that we can have a
  //   more performant impl.
  // The @OSID annotation simple indicates that this is
  //   an OSID method.
  @OSID
  @Override
  public JournalEntry getJournalEntry(Id journalEntryId)
      throws OperationFailedException, org.osid.PermissionDeniedException, NotFoundException {

    // Here we grab a JournalEntryQuery and use it to
    //   do lookup. It does assume certain methods
    //   are implemented in the Query and the QuerySession.

    // Note that we tend to provide fully qualified
    //   class paths inline, instead of importing
    //   at the top of the file and calling just
    //   "JournalEntry" here. That way we avoid any
    //   confusion with the package's JournalEntry class
    //   and the org.osid JournalEntry interface.
    Optional<JournalEntry> journalEntry =
        allJournalEntries.stream().filter(r -> r.getId().equals(journalEntryId)).findAny();
    JournalEntry obj;
    if (journalEntry.isPresent()) {
      obj = journalEntry.get();
    } else {
      throw new NotFoundException("Could not find the JournalEntry: " + journalEntryId);
    }
    return obj;
  }

  // If we do not need to override the Jamocha implementation, then
  //    we can leave these methods out or just call super().
  @Override
  public org.osid.journaling.JournalEntryList getJournalEntriesByIds(
      org.osid.id.IdList journalEntryIds)
      throws NotFoundException, OperationFailedException, org.osid.PermissionDeniedException {

    // NOTE: Every passed in Id must return at most one
    //       unique entity (or none). Each given Id should
    //       NEVER refer to multiple rows in the system of record.
    // NOTE: Per the spec, Ids can be repeated in the arguments,
    //       which may result in duplicate results returned
    //       (depending on plenary vs. comparative).
    // Read this wiki article for more information:
    //    https://dxtera.atlassian.net/wiki/spaces/TEC/pages/250806281/Sanity+With+Ids

    List<Id> ids = new ArrayList<>();
    while (journalEntryIds.hasNext()) {
      Id id = journalEntryIds.getNextId();
      ids.add(id);
    }
    List<JournalEntry> journalEntriesFilteredById =
        allJournalEntries
            .stream()
            .filter(r -> ids.contains(r.getId()))
            .collect(Collectors.toList());
    return new ArrayJournalEntryList(journalEntriesFilteredById);
  }

  // If we do not need to override the Jamocha implementation, then
  //    we can leave these methods out or just call super().
  @Override
  public org.osid.journaling.JournalEntryList getJournalEntriesByGenusType(org.osid.type.Type type)
      throws OperationFailedException, org.osid.PermissionDeniedException {

    List<JournalEntry> journalEntryFilteredByType =
        allJournalEntries
            .stream()
            .filter(r -> r.getGenusType().equals(type))
            .collect(Collectors.toList());
    return new ArrayJournalEntryList(journalEntryFilteredByType);
  }

  // If we do not need to override the Jamocha implementation, then
  //    we can leave these methods out or just call super().
  // @Override
  // public org.osid.journaling.JournalEntryList
  // getJournalEntriesByParentGenusType(org.osid.type.Type journalEntryGenusType)
  //     throws org.osid.OperationFailedException,
  //            org.osid.PermissionDeniedException {

  //   return super.getJournalEntriesByParentGenusType(journalEntryGenusType);
  // }

  // If we do not need to override the Jamocha implementation, then
  //    we can leave these methods out or just call super().
  // @Override
  // public org.osid.journaling.JournalEntryList getJournalEntriesByRecordType(org.osid.type.Type
  // journalEntryRecordType)
  //     throws org.osid.OperationFailedException,
  //            org.osid.PermissionDeniedException {

  //   return super.getJournalEntriesByRecordType(journalEntryRecordType);
  // }

  @Override
  public org.osid.journaling.JournalEntryList getJournalEntriesByDateForBranch(
      Id branchId, org.osid.calendaring.DateTime d)
      throws OperationFailedException, org.osid.PermissionDeniedException {

    return new ArrayJournalEntryList(new ArrayList<JournalEntry>());
  }

  // This is the only required method impl to get the
  //   abstract Jamocha LookupSessions to work.
  // The @OSID annotation simple indicates that this is
  //   an OSID method.
  @OSID
  @Override
  // The JournalEntryList that is returned should be an iterator so that
  //   we don't try to unpack all the results into memory.
  public org.osid.journaling.JournalEntryList getJournalEntries()
      throws OperationFailedException, org.osid.PermissionDeniedException {

    // Here we avoid the Jamocha JDBC List iterators because they seem
    //   to hang if the buffer size is set too small relative to
    //   the number of rows. We use the SIS impls instead, or you
    //   can create your own iterators.

    return new ArrayJournalEntryList(allJournalEntries);
  }
}
