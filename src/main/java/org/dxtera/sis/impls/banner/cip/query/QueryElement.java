package org.dxtera.sis.impls.banner.cip.query;

public class QueryElement {
  String type;
  String id;
  Boolean match;
  String datatype;

  public QueryElement(String type, String id, Boolean match, String datatype) {
    this.type = type;
    this.id = id;
    this.match = match;
    this.datatype = datatype;
  }

  public String getType() {
    return this.type;
  }

  public String getId() {
    return this.id;
  }

  public Boolean getMatch() {
    return this.match;
  }

  public String getDataType() {
    return this.datatype;
  }
}
