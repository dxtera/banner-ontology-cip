package org.dxtera.sis.impls.banner.cip.ontology;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import net.okapia.osid.jamocha.ontology.subject.ArraySubjectList;
import org.dxtera.sis.impls.banner.csv.CsvService;
import org.dxtera.sis.impls.banner.csv.beans.CipBean;
import org.osid.NotFoundException;
import org.osid.OperationFailedException;
import org.osid.binding.java.annotation.OSID;
import org.osid.id.Id;
import org.osid.ontology.Subject;

// Scott Thorne (Templated by Jeff Merriman, Cole Shaw, Amon Horne)
// DXtera
// February 2017
//
// Copyright (c) 2017 DXtera Institute. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicense, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

// Typically we want to extend from net.okapia.osid.jamocha where possible,
// since that package contains a lot of helpful Abstract base
// classes and implementations.
// We always want to state which OSID interfaces we are implementing.
public class BannerSubjectLookupSession
    extends net.okapia.osid.jamocha.ontology.spi.AbstractSubjectLookupSession
    implements org.osid.ontology.SubjectLookupSession {

  protected BannerOntologyInterface catalog;

  // This is the SubjectQuerySession that this SubjectLookupSession
  //    will be built upon.
  protected BannerOntologyManager mgr;
  private List<Subject> allSubjects;

  // We use our BannerOntologyInterface for the catalog
  //   type here, instead of the generic OSID Ontology
  //   interface, since we know these implementations are tied
  //   together.
  public BannerSubjectLookupSession(BannerOntologyManager mgr) throws OperationFailedException {

    super();
    this.mgr = mgr;
    setOntology(mgr.catalog);
    this.catalog = mgr.catalog;
    CsvService csvService = new CsvService();
    List<CipBean> cipCodes = csvService.getCIPCodes();
    this.allSubjects =
        cipCodes
            .stream()
            .map(
                cips -> {
                  try {
                    return this.catalog.getSubjectGenerator().convert(cips);
                  } catch (OperationFailedException e) {
                    e.printStackTrace();
                    return null;
                  }
                })
            .filter(Objects::nonNull)
            .collect(Collectors.toList());
    return;
  }

  @Override
  public boolean canLookupSubjects() {
    return true;
  }

  // We override the Jamocha impl of getSubject(),
  //   typically for performance reasons. Jamocha iterates through
  //   all the Subjects to find an Id match, and
  //   using direct SQL queries we know that we can have a
  //   more performant impl.
  // The @OSID annotation simple indicates that this is
  //   an OSID method.
  @OSID
  @Override
  public org.osid.ontology.Subject getSubject(org.osid.id.Id subjectId)
      throws OperationFailedException, org.osid.PermissionDeniedException, NotFoundException {

    // Here we grab a SubjectQuery and use it to
    //   do lookup. It does assume certain methods
    //   are implemented in the Query and the QuerySession.

    // Note that we tend to provide fully qualified
    //   class paths inline, instead of importing
    //   at the top of the file and calling just
    //   "Subject" here. That way we avoid any
    //   confusion with the package's Subject class
    //   and the org.osid Subject interface.

    Optional<Subject> subject =
        allSubjects.stream().filter(s -> s.getId().equals(subjectId)).findAny();
    org.osid.ontology.Subject obj;
    if (subject.isPresent()) {
      obj = subject.get();
    } else {
      throw new NotFoundException("Could not find the Subject: " + subjectId);
    }
    return obj;
  }

  // If we do not need to override the Jamocha implementation, then
  //    we can leave these methods out or just call super().
  @Override
  public org.osid.ontology.SubjectList getSubjectsByIds(org.osid.id.IdList subjectIds)
      throws NotFoundException, OperationFailedException, org.osid.PermissionDeniedException {

    // NOTE: Every passed in Id must return at most one
    //       unique entity (or none). Each given Id should
    //       NEVER refer to multiple rows in the system of record.
    // NOTE: Per the spec, Ids can be repeated in the arguments,
    //       which may result in duplicate results returned
    //       (depending on plenary vs. comparative).
    // Read this wiki article for more information:
    //    https://dxtera.atlassian.net/wiki/spaces/TEC/pages/250806281/Sanity+With+Ids

    List<Id> ids = new ArrayList<>();
    while (subjectIds.hasNext()) {
      Id id = subjectIds.getNextId();
      ids.add(id);
    }
    List<Subject> subjectsFilteredById =
        allSubjects.stream().filter(r -> ids.contains(r.getId())).collect(Collectors.toList());
    return new ArraySubjectList(subjectsFilteredById);
  }

  // If we do not need to override the Jamocha implementation, then
  //    we can leave these methods out or just call super().
  @Override
  public org.osid.ontology.SubjectList getSubjectsByGenusType(org.osid.type.Type type)
      throws OperationFailedException, org.osid.PermissionDeniedException {

    List<Subject> subjectsFilteredByType =
        allSubjects
            .stream()
            .filter(r -> r.getGenusType().equals(type))
            .collect(Collectors.toList());
    return new ArraySubjectList(subjectsFilteredByType);
  }

  // If we do not need to override the Jamocha implementation, then
  //    we can leave these methods out or just call super().
  // @Override
  // public org.osid.ontology.SubjectList getSubjectsByParentGenusType(org.osid.type.Type
  // subjectGenusType)
  //     throws org.osid.OperationFailedException,
  //            org.osid.PermissionDeniedException {

  //   return super.getSubjectsByParentGenusType(subjectGenusType);
  // }

  // If we do not need to override the Jamocha implementation, then
  //    we can leave these methods out or just call super().
  // @Override
  // public org.osid.ontology.SubjectList getSubjectsByRecordType(org.osid.type.Type
  // subjectRecordType)
  //     throws org.osid.OperationFailedException,
  //            org.osid.PermissionDeniedException {

  //   return super.getSubjectsByRecordType(subjectRecordType);
  // }

  // This is the only required method impl to get the
  //   abstract Jamocha LookupSessions to work.
  // The @OSID annotation simple indicates that this is
  //   an OSID method.
  @OSID
  @Override
  // The SubjectList that is returned should be an iterator so that
  //   we don't try to unpack all the results into memory.
  public org.osid.ontology.SubjectList getSubjects()
      throws OperationFailedException, org.osid.PermissionDeniedException {

    // Here we avoid the Jamocha JDBC List iterators because they seem
    //   to hang if the buffer size is set too small relative to
    //   the number of rows. We use the SIS impls instead, or you
    //   can create your own iterators.

    return new ArraySubjectList(allSubjects);
  }
}
