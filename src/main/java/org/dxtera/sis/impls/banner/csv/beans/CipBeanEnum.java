/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dxtera.sis.impls.banner.csv.beans;

/** @author alanh */
public enum CipBeanEnum {
  CIPFAMILY(0),
  CIPCODE(1),
  ACTION(2),
  TEXTCHANGE(3),
  CIPTITLE(4),
  CIPDEFINITION(5),
  CIPCROSSREFERENCES(6),
  EXAMPLES(7);
  private final int index;

  CipBeanEnum(int index) {
    this.index = index;
  }

  public int getIndex() {
    return index;
  }
}
