package org.dxtera.sis.impls.banner.cip.ontology.type;

/**
 * id and namespace constants to use for ids of different types
 *
 * @author nwright
 */
public class IdNamespaceConstants {

  public static final String CALENDAR = "Calendar";
  public static final String COORDINATE = "Coordinate";
  public static final String ONTOLOGY = "Ontology";
  public static final String JOURNAL = "Journal";
  public static final String BRANCH = "Branch";
  public static final String SUBJECT = "Subject";
  public static final String RELEVANCY = "Relevancy";
  public static final String JOURNAL_ENTRY = "JournalEntry";
  public static final String TIME = "Time";
  public static final String CIP_ID_PREFIX = "cip.2010.";
  public static final String HIERARCHY = "Hierarchy";
}
