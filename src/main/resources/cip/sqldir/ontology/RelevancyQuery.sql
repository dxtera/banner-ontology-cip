(select distinct smr.smrprle_program || '-' || stv.cipcode id,
'Program Id ' || smr.smrprle_program || ' to CIP Code ' || stv.cipcode display_name,
'Program Id ' || smr.smrprle_program || ' -- maps to CIP Code ' || stv.cipcode description,
'program.2.cip.2010' pt_id,
to_date('01-01-1000','MM-DD-YYYY') start_date,
to_Date('12-31-9999','MM-DD-YYYY') end_date,
stv.cipcode subject,
 'Program:'|| smr.smrprle_program || '@ccsnh' mapped from smrprle smr inner join sobcurr sob on sob.sobcurr_program = smr.smrprle_program inner join sorcmjr sor on sor.sorcmjr_curr_rule = sob.sobcurr_curr_rule inner join
(select case when stvmajr_cipc_code not like '%.%' and length(stvmajr_cipc_code) > 2
             then  'cip.2010.' || substr(stvmajr_cipc_code,1,2) || '.' || substr(stvmajr_cipc_code, 3)
             when stvmajr_cipc_code is null
             then 'cip.2010.'
             else  'cip.2010.' || stvmajr_cipc_code
        end cipcode
     , stvmajr.* from stvmajr) stv on sor.sorcmjr_majr_code = stv.stvmajr_code
) a
