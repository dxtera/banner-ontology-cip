(select distinct smr.smrprle_surrogate_id || '-' || stv.cipcode  id,
 'Program Id ' || smr.smrprle_surrogate_id || ' to CIP Code ' || stv.cipcode display_name,
 'Program Id ' || smr.smrprle_surrogate_id || ' -- maps to CIP Code ' || stv.cipcode description,
                 'BannerDefaultCIP' pt_id,
                 'BannerCIPBranch' branch,
                 smr.smrprle_surrogate_id || '-' || stv.cipcode source,
                 smrprle_activity_date eventdate from smrprle smr inner join sobcurr sob on sob.sobcurr_program = smr.smrprle_program
                 inner join sorcmjr sor on sor.sorcmjr_curr_rule = sob.sobcurr_curr_rule
                 inner join (select case when stvmajr_cipc_code not like '%.%' and length(stvmajr_cipc_code) > 2
                                             then substr(stvmajr_cipc_code, 1, 2) || '.' || substr(stvmajr_cipc_code ,3)
                                          when stvmajr_cipc_code is null
                                             then 'cip.2010.'
                                         else stvmajr_cipc_code
                                         end cipcode
     , stvmajr.* from stvmajr) stv on sor.sorcmjr_majr_code = stv.stvmajr_code  ) a  
