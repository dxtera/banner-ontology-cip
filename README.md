A lookup-only ontology provider.

Make sure to add the right database connection information to a `org.dxtera.sis.impls.banner.ontology.GenericOntologyManager.xml` file in the classpath (or equivalent file if you use a different manager class name). A skeleton file is included at `src/main/resources/org.dxtera.sis.impls.banner.ontology.GenericOntologyManager.xml.skel` for reference.

Pretty sure tests won't run until we get a database set up and connection defined.