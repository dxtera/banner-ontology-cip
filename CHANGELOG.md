# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.0.4] - 2020-02-07

### Changed

- `FRAM-227`: Update SQL and tests for just ethnicity (remove race).

## [0.0.3] - 2020-01-30

### Changed

- `FRAM-225`: Fixed convert methods in Relevancy.

## [0.0.2] - 2020-01-29

### Added

- `FRAM-224`: Change package paths; add handcoded tests.

## [0.0.1] - 2020-01-28

### Added

- `FRAM-206`: Initial stubs.

For previous versions, please check the commit logs...

[Unreleased]: https://bitbucket.org/dxtera/banner-ontology-ethnicity/compare/v0.0.4...HEAD
[0.0.4]: https://bitbucket.org/dxtera/banner-ontology-ethnicity/compare/v0.0.3...v0.0.4
[0.0.3]: https://bitbucket.org/dxtera/banner-ontology-ethnicity/compare/v0.0.2...v0.0.3
[0.0.2]: https://bitbucket.org/dxtera/banner-ontology-ethnicity/compare/v0.0.1...v0.0.2
[0.0.1]: https://bitbucket.org/dxtera/banner-ontology-ethnicity/releases/v0.0.1
